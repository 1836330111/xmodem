#ifndef __XMODEM_H__
#define __XMODEM_H__

#include "stm8l15x.h"
#include "string.h"
#include "boot.h"
#include "usart.h"
#include "stm8l15x_flash.h"
#include "stdio.h"
#include "stm8l15x_usart.h"
/* /--------------------------------- Packet in IAP memory ---------------------------------\
 * |      0       |        1      |         2        |  Byte4---  Byte1027   | checksum  | 
 * |------------------------------------------------------------------------|
 * | Start header | packet number | ~(packet number) |     PacketDate        | checksum |
 * \----------------------------------------------------------------------------------------/
 *                                                                                          */

#define SOH             0x01 //帧起始头128

#define STX             0x02 //帧起始头1K

#define EOT             0x04 //结束文件传输

#define ACK             0x06 //应答

#define NACK             0x15 //非应答

#define CAN             0x18 //取消文件传输

#define Xmodem_1K       1024  //传输数据长度

#define Xmodem_128       128  //传输数据长度

#define Checksum_Mode     1   //校验和模式一个字节，超过255数值-255
#define Checkcrc_Mode     2   //CRC校验模式

#define PacketInfo_len 3  //数据包前3个数据，例如  SOH 01 0xFE


//注意DataBuff[]中的参数：128字节传输选择Xmodem_128；1K字节传输选择Xmodem_1K
//注意DataBuff[]中的参数：128字节传输选择Xmodem_128；1K字节传输选择Xmodem_1K
typedef struct
{
  uint8_t Block;       //下一次的块号
  uint8_t DataBuff[PacketInfo_len+Xmodem_1K+Checksum_Mode];  //注意
 const USART1_Interface *IapFun;  //IAP需要用到的函数
}
Xmode;  //Xmodem 控制结构体


//Xmodem 控制结构体
extern Xmode   XmodemControl;

//IAP初始化    装载需要用到的函数
void IAP_XmodemDownload_Init(const USART1_Interface *BootInter);

//执行IAP
void IAP_XmodemDownload(void);



#endif

