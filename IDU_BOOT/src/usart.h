#ifndef _USART_H_
#define _USART_H_

#include "stm8l15x_usart.h"
#include "stdio.h"
#include <string.h>

#define def_baudrate    115200

int fputc(int ch, FILE *f);//可以直接使用printf通过串口打印信息
void usart_init(void);//usart初始化
void USART1_SendBytes(u8 *Buff,u16 Len);//USART1 发送Len个字符
void USART1_SendByte(u8 Byte);//USART1发送一个字节

uint8_t USART1_Receive(u8* ReceiveData,u32 Timeout);//串口1接收数据 超时退出
#endif
