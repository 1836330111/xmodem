#include "stm8l151c8.h"
#include "stm8l15x_clk.h"
#include "usart.h"

//extern uint8_t Laser_cmd_req ;
//extern uint8_t UartTx_buff[20];
//extern uint8_t Uart1_Send_Count;
extern uint32_t Time2_count;

extern void usart_init(void);

void clk_init(void)
{
	CLK_DeInit();
	//CLK_SYSCLKSourceSwitchCmd(ENABLE);
	//CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSE);
	CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
	while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI);
	CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);

}


void gpio_init(void)
{

		GPIO_Init(GPIOC , GPIO_Pin_2 , GPIO_Mode_In_FL_No_IT);
		GPIO_Init(GPIOC , GPIO_Pin_3 , GPIO_Mode_Out_PP_High_Fast); 
		GPIO_Init(GPIOE , GPIO_Pin_6 , GPIO_Mode_In_FL_No_IT); 
		GPIO_Init(GPIOC , GPIO_Pin_6 , GPIO_Mode_Out_PP_Low_Slow); 
		GPIO_Init(GPIOC , GPIO_Pin_4 , GPIO_Mode_Out_PP_Low_Slow); 

		GPIO_Init(GPIOC , GPIO_Pin_0 , GPIO_Mode_Out_PP_Low_Slow); 
		GPIO_Init(GPIOC , GPIO_Pin_1 , GPIO_Mode_Out_PP_Low_Slow); 
		GPIO_Init(GPIOD , GPIO_Pin_5 , GPIO_Mode_Out_PP_Low_Slow); 
		GPIO_Init(GPIOD , GPIO_Pin_4 , GPIO_Mode_In_FL_No_IT); 
		GPIO_Init(GPIOD , GPIO_Pin_0 | GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast); 
		GPIO_Init(GPIOD , GPIO_Pin_2 , GPIO_Mode_Out_PP_High_Fast); 
		GPIO_Init(GPIOE , GPIO_Pin_0 , GPIO_Mode_Out_PP_Low_Slow); 

		GPIO_Init(GPIOB ,GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3, GPIO_Mode_In_FL_No_IT);

		GPIO_Init(GPIOE , GPIO_Pin_1 , GPIO_Mode_Out_PP_High_Fast); 
		GPIO_Init(GPIOE , GPIO_Pin_2 , GPIO_Mode_Out_PP_High_Fast);
		//GPIO_Init(GPIOE , GPIO_Pin_7 , GPIO_Mode_Out_PP_High_Fast);

		GPIO_ResetBits(GPIOE , GPIO_Pin_0);
		GPIO_ResetBits(GPIOD , GPIO_Pin_5);
		GPIO_ResetBits(GPIOD , GPIO_Pin_0);
		GPIO_ResetBits(GPIOC , GPIO_Pin_4);
		GPIO_ResetBits(GPIOC , GPIO_Pin_6);
		GPIO_SetBits(GPIOD , GPIO_Pin_2);
		GPIO_SetBits(GPIOD , GPIO_Pin_1);

}


void tim2_init(void)
{
  TIM2_DeInit();
  TIM2_TimeBaseInit(TIM2_Prescaler_4 , TIM2_CounterMode_Up , TIM2_Period);
  TIM2_ClearFlag(TIM2_FLAG_Update);
  //TIM2_ITConfig(TIM2_IT_Update , ENABLE);
  TIM2_SetCounter(0);
  TIM2_Cmd(ENABLE);
}


void hardware_init(void)
{	
	clk_init();
	gpio_init();
	tim2_init();
	usart_init();
	disableInterrupts();//IAP Disable interrrupt for usart  loop recieve data
}
void delayms(uint32_t n)
{
	int i;
	for(i=0;i<n;i++)
	{
		TIM2_SetCounter(0);
		Time2_count=0;
		while(Time2_count<=4000)
		{
			Time2_count = TIM2_GetCounter();
		}

	}

}
uint8_t Com_checkSum(uint8_t* pbuf)
{
	uint8_t i,sum = 0;
	for(i=0;i<19;i++)
		sum += pbuf[i];
	return sum&0xff;
}

uint8_t Uart_Comm_Check(uint8_t u8Head)
{
		 uint8_t u8HeadCheck=u8Head;
		 uint8_t u8Ret=TRUE;
		 switch(u8HeadCheck&0x0F)
		 {
			 case TARGET_HOST:// to computer
			 case TARGET_DP9320:// to STDP9320
			 case TARGET_IDU:// to IDU
			 case TARGET_DDP:// to DDP1
			 case TARGET_DDP2:// to DDP2
			 case TARGET_STM32:// to MCU
				 break;
			 default:
				 u8Ret = FALSE;
				 break;
		 }
		 
		 switch((u8HeadCheck&0xF0)>>4)
		 {
	 
			 case TARGET_HOST:// to computer
			 case TARGET_DP9320:// to STDP9320
			 case TARGET_IDU:// to IDU
			 case TARGET_DDP:// to DDP1
			 case TARGET_DDP2:// to DDP2
			 case TARGET_STM32:// to MCU
				 break;
			 default:
				 u8Ret = FALSE;
				 break;
		 }
		 
		 if((u8HeadCheck&0xF) == ((u8HeadCheck&0xF0)>>4))
		 {
			 u8Ret = FALSE;
		 }
		 return u8Ret;
}

