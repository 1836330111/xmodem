/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/*****************************************************************************
支持printf函数通过串口直接打印
备注：  IAR中需要开启library configuration->FULL
*******************************************************************************/
int fputc(int ch, FILE *f)//printf
{
	USART_SendData8(USART1, (uint8_t) ch);

	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {}	
   
    return ch;
}

/*******************************************************************************
**函数名： void usart_init(u32 Baud)
**输入：	波特率 9600/115200/
**输出：	无
**功能描述：1.串口初始化
            2.开启了串口时钟
            3.PC3 推挽输出   PC2上拉输入不带中断  
**全局变量：
**调用模块：GPIO、USART、CLK初始化
*******************************************************************************/
void usart_init(void)
{
   //串口初始化
  USART_Init(USART1,def_baudrate,USART_WordLength_8b,USART_StopBits_1,USART_Parity_No,(USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
       
 // USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//启动接收中断
  
  USART_Cmd(USART1, ENABLE);//启动UASRT1

}

//串口1接收数据 超时退出
uint8_t USART1_Receive(uint8_t* ReceiveData,uint32_t Timeout)
{
   while(Timeout--)
   {
     if(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET)//接收标志位为1 表示收到数据      
     {
       *ReceiveData=USART1->DR;
       return 0;
         
     }
     Timeout--;  
   }
   return 1; 

}

//USART1发送一个字节
void USART1_SendByte(uint8_t Byte)
{
  
  USART_SendData8(USART1, Byte);
  while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET); 	
  
}

//USART1 发送多个字节
void USART1_SendBytes(uint8_t *Buff,uint16_t Len)
{

    while(Len--)
    {
    
      USART1_SendByte(*Buff);
      Buff++;
       
    }  

}














