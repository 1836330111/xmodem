#include "xmodem.h"

//Xmodem 控制结构体
Xmode   XmodemControl;

//Xmodem累加校验
uint8_t XmodemAddCRC_Check( const unsigned char *buf, uint16_t length)
{
  uint16_t Count;
  uint8_t  CRCdata = 0 ; 
  for(Count = 0 ; Count < length ;Count ++)
  {
    CRCdata += buf[Count] ; 
  }
  return (CRCdata & 0xff);
}


//IAP初始化装载需要用到的函数
void IAP_XmodemDownload_Init(const USART1_Interface *BootInter)
{
  XmodemControl.IapFun = BootInter;
}


void IAP_XmodemDownload(void)
{
  uint8_t RecData = 0 ;
  uint16_t Count = 0 ;
  //写当前FLASH地址 
  uint32_t IAPFlashAddr = MAIN_USER_Start_ADDR;
  
  XmodemControl.IapFun->USART1_SendByte(NACK);   //发送一个非应答表示为checksum校验方式
  
  if(XmodemControl.IapFun->USART1_Receive(&RecData,10000)==0) //如果有收到数据
  {

    if(RecData==STX)  //如果收到数据帧起始信号
    {     
      XmodemControl.DataBuff[0]=RecData;
      XmodemControl.Block = 1 ;  //刚开始接收 标记块号为 1
      //如果接收到帧起始头
      while(XmodemControl.DataBuff[0]==STX)
      {
        for(Count = 1 ; Count < (PacketInfo_len+Xmodem_1K+Checksum_Mode) ; Count++ )
        {
          XmodemControl.IapFun->USART1_Receive(&XmodemControl.DataBuff[Count],10000);  //接收接下来的 1027个字节数据
        } 
        //块号与 块反码       
        if(( XmodemControl.DataBuff[1]+ XmodemControl.DataBuff[2]==0XFF )&&
           (XmodemControl.DataBuff[1] == XmodemControl.Block ) &&       //块号与上一个块号
             (XmodemAddCRC_Check(&XmodemControl.DataBuff[3], Xmodem_1K) == XmodemControl.DataBuff[PacketInfo_len+Xmodem_1K+Checksum_Mode-1] )) //累加校验值核对
//         if(1) 
        {
          XmodemControl.Block++;
          for( Count=0 ; Count<Xmodem_1K ; Count++ ,IAPFlashAddr++ )
          {
            FLASH_ProgramByte(IAPFlashAddr,XmodemControl.DataBuff[3+Count]); //写FLASH数据
          }
          XmodemControl.IapFun->USART1_SendByte(ACK);  //请求下一个包
        }
        else
        {
          //请求重发
          XmodemControl.IapFun->USART1_SendByte(NACK);       
        }
        //接收数据帧的起始头   必须接收到
        while(1)
        {
          
          if(XmodemControl.IapFun->USART1_Receive(&XmodemControl.DataBuff[0],10000)==0) //如果有收到数据
          {
            if(XmodemControl.DataBuff[0]==STX) //如果收到起始头
            {
              break;
            }
            else if(XmodemControl.DataBuff[0]==EOT)  //文件传输完成了 
            {
              XmodemControl.IapFun->USART1_SendByte(ACK);  //发送应答
              
              //如果用户APP的起始地址 为跳转命令 则认为APP代码是正常的
              if((*((uint8_t FAR*)MAIN_USER_Start_ADDR)==0x82) || (*((u8 FAR*)MAIN_USER_Start_ADDR)==0xAC))
              {
                FLASH_EraseByte(MAIN_USER_Start_ADDR-1);   //写入APP更新完成标记
                printf("\n go to app Succeed!\n");   
                goto_app();
              }	
              printf("\n go to app Failed!\n");                 
              return;
            }
            else  //其它数据  认为终止传输   则会重新开始
            {
              return;
            }         
          } 
        }     
      }
    }    
  }      
}






