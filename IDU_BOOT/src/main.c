//open with VScode watch code better
#include "stm8l15x.h"
#include "stm8l151c8.h"
#include "stm8l15x_flash.h"
#include "xmodem.h"
#if 1
/* needed by memcpy for raisonance */
#include <string.h>
extern int __address__FLASH_EraseBlock;
extern int __size__FLASH_EraseBlock;
extern int __address__FLASH_ProgramBlock;
extern int __size__FLASH_ProgramBlock;
#endif /*_RAISONANCE_*/
uint8_t UpdateFlg;//更新标志位
void main (void)
{
	hardware_init();//硬件初始化
        STM8_HanderIqr_Init();//重定向中断向量表  非常重要！！！
        FLASH_Unlock(FLASH_MemType_Program);//解锁flash
        IAP_XmodemDownload_Init(&BootInterface);//Xmodem初始化   挂载IAP 需要用到的函数 
        UpdateFlg = FLASH_ReadByte((uint32_t)(MAIN_USER_Start_ADDR-1));//读取标志位地址的值
	while(1)
	{
      if(UpdateFlg==Update_BIT)//如果用户代码更新完成标记存在  则表示当前已经有APP代码  直接运行
      {
            printf("goto  app");
            UpdateFlg=0;  //清除更新标志位

            goto_app();  //运行APP
      }   
      else
      {
        FLASH_EraseByte(MAIN_USER_Start_ADDR-1);
        FLASH_ProgramByte(MAIN_USER_Start_ADDR-1,Update_BIT);   //写入APP更新完成标记
        FLASH_Lock(FLASH_MemType_Program);//解锁flash
        FLASH_Unlock(FLASH_MemType_Program);//解锁flash
        IAP_XmodemDownload();//接收上位机《超级终端》发送的bin文件 -> 写入Flash ->跳转执行APP   
      }
	}
 }
 
	
