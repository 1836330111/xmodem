/**
  ******************************************************************************
  * @file    stm8l15x_flash.c
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    28-June-2013
  * @brief   This file provides all the Flash firmware functions. These functions 
  *          can be executed from Internal FLASH or Internal RAM memories.
  *            - FLASH program and Data EEPROM memories interface configuration
  *            - FLASH program and Data EEPROM memories Programming
  *            - Option Bytes Programming
  *            - Interrupts and flags management
  *            - Functions to be executed from RAM
  *               
  *  @verbatim
  *  
  *          ===================================================================
  *                                 How to use this driver
  *          ===================================================================
  *                           
  *          This driver provides functions to configure and program the Flash 
  *          memory of all STM8L15x devices
  *          These functions are split in 4 groups
  * 
  *           1. FLASH program and Data EEPROM memories interface configuration 
  *              functions: this group includes the management of the following 
  *              features:
  *                    - Set the fixed programming time
  *                    - Configure the power state during WFI mode
  *                    - Configure the power state during run, low power run and
  *                      WFE modes (should be executed from internal RAM) 
  *  
  *           2. FLASH program and Data EEPROM memories Programming functions: 
  *              this group includes all needed functions to erase and program 
  *              the FLASH program or the Data EEPROM memory.
  *                    - Lock and Unlock the Flash program interface.
  *                    - Lock and Unlock the Data EEPROM interface.  
  *                    - Erase function: Erase Byte, Erase Word and Erase Block 
  *                      (should be executed from internal RAM). 
  *                    - Program functions: Program Byte, Program Word, 
  *                      Program Block (should be executed from internal RAM) 
  *                      and Fast Program Block (should be executed from internal
  *                      RAM).
  *    
  *           3. FLASH Option Bytes Programming functions: this group includes 
  *              all needed functions to:
  *                    - Program/erase the user option Bytes 
  *                    - Get the Read Out Protection status (ROP option byte)
  *                    - Get the User Boot Code size (UBC option byte)
  *                    - Get the Protected Code size (PCODE option byte) 
  *                    
  *            Note: Option byte programming is very similar to data EEPROM byte
  *                  programming.          
  *  
  *           4. FLASH Interrupts and flag management functions: this group 
  *              includes all needed functions to:
  *                    - Enable/Disable the flash interrupt sources
  *                    - Get flags status
  *                    - Wait for last flash operation(can be executed from 
  *                      internal RAM)
  *                        
  *           5. Functions to be executed from RAM: this group includes the functions
  *              that should be executed from RAM and provides description on how 
  *              to handle this with the different supported toolchains
  *   
  *  The table below lists the functions that can be executed from RAM.
  *  
  *   +--------------------------------------------------------------------------------|
  *   |   Functions prototypes      |    RAM execution            |     Comments       |
  *   ---------------------------------------------------------------------------------|
  *   |                             | Mandatory in case of block  | Can be executed    |
  *   | FLASH_WaitForLastOperation  | Operation:                  | from Flash in case |
  *   |                             | - Block programming         | of byte and word   |
  *   |                             | - Block erase               | Operations         |
  *   |--------------------------------------------------------------------------------|
  *   | FLASH_PowerRunModeConfig    |       Exclusively           | useless from Flash |
  *   |--------------------------------------------------------------------------------|
  *   | FLASH_GetPowerStatus        |       Exclusively           | useless from Flash |
  *   |--------------------------------------------------------------------------------|
  *   | FLASH_ProgramBlock          |       Exclusively           | useless from Flash |
  *   |--------------------------------------------------------------------------------|
  *   | FLASH_EraseBlock            |       Exclusively           | useless from Flash |
  *   |--------------------------------------------------------------------------------|
  *  
  *  To be able to execute functions from RAM several steps have to be followed.
  *   These steps may differ from one toolchain to another.
  *   A detailed description is available below within this driver.
  *   You can also refer to the Flash_DataProgram example provided within the
  *   STM8L15x_StdPeriph_Lib package.
  * 
  *  @endverbatim
  *                      
  ******************************************************************************
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ****************************************************************************** 
  */
/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_flash.h"
#include "stm8l151c8.h"

/** @addtogroup STM8L15x_StdPeriph_Driver
  * @{
  */

/** @defgroup FLASH 
  * @brief FLASH driver modules
  * @{
  */
  
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/** @defgroup FLASH_Private_Define
  * @{
  */ 
#define FLASH_CLEAR_BYTE   ((uint8_t)0x00)
#define FLASH_SET_BYTE     ((uint8_t)0xFF)
#define OPERATION_TIMEOUT  ((uint16_t)0xFFFF)
/**
  * @}
  */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
 
/** @defgroup FLASH_Private_Functions
  * @{
  */ 

/** @defgroup FLASH_Group1 FLASH program and Data EEPROM memories Interface 
  *                        configuration functions
  *  @brief   FLASH Interface configuration functions 
 *
@verbatim   
 ===============================================================================
      FLASH program and Data EEPROM memories interface configuration functions
 ===============================================================================  

   The FLASH program and Data EEPROM memories interface configuration functions,
    includes the following functions:
		
   - FLASH_ProgramTime_TypeDef FLASH_GetProgrammingTime(void);
   - void FLASH_SetProgrammingTime(FLASH_ProgramTime_TypeDef FLASH_ProgTime);
	 - void FLASH_PowerWaitModeConfig(FLASH_Power_TypeDef FLASH_Power);

   These functions don't need the unlock sequence.

@endverbatim
  * @{
  */
#if 1
extern uint32_t pBufPartIndex,pBufPartSize,pBufPartCRC;
extern uint32_t file_size,file_crc;
extern uint8_t UartRx_buff[20];

uint32_t update_sw_app_addr;
uint32_t update_sw_Time;
uint8_t RX_Words;
uint8_t usart1_sr;

extern uint8_t psendbufpart[2048];
extern uint8_t sendbufglag;//0-> noaml 1-> send sw
extern uint32_t pGetBufIndex;
extern uint8_t Laser_cmd_req;
uint8_t   	 paste_start_block_APP = 130; //flash开始块区((c100 - 8000 )/((17FFF - 8000)/FLASH_PROGRAM_BLOCKS_NUMBER))
uint32_t crc11,crc22;
const u32 CRC32_table[256] =
{
  0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
  0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
  0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
  0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
  0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
  0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
  0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
  0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
  0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
  0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
  0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
  0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
  0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
  0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
  0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
  0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
  0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
  0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
  0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
  0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
  0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
  0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
  0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
  0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
  0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
  0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
  0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
  0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
  0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
  0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
  0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
  0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
  0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
  0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
  0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
  0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
  0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
  0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
  0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
  0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
  0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
  0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
  0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
  0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
  0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
  0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
  0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
  0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
  0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
  0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
  0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
  0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
  0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
  0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
  0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
  0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
  0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
  0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
  0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
  0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
  0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
  0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
  0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
  0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};
void (**AppEntry)(void) = (void(**)(void))0x0000c104;

uint32_t GetCRC32(uint8_t* buf, uint32_t len)
{
  uint32_t CRC32_data = 0xFFFFFFFF;
  for (uint32_t i = 0; i != len; ++i)
  {
    uint32_t t = (CRC32_data ^ buf[i]) & 0xFF;
    CRC32_data = ((CRC32_data >> 8) & 0xFFFFFF) ^ CRC32_table[t];
  }
  return ~CRC32_data;
}
uint32_t GetCRC32_2(uint32_t Address, uint32_t len)
{
  uint32_t CRC32_data = 0xFFFFFFFF;
  for (uint32_t i = 0; i != len; ++i)
  {
    uint32_t t = (CRC32_data ^ FLASH_ReadByte(Address+i)) & 0xFF;
    CRC32_data = ((CRC32_data >> 8) & 0xFFFFFF) ^ CRC32_table[t];
  }
  return ~CRC32_data;
}

uint8_t Usart1_data_write(void)
{
    usart1_sr=USART1->SR;
    while(!(usart1_sr&0x20))
    {
      usart1_sr=USART1->SR;
    }
    if((usart1_sr&0x08)||(usart1_sr&0x01))
    {
      return 0;
    }
    else
    {  
      return USART1->DR;
    }  
}


void  Usart_Receive_Func(void)
{
	static uint8_t index = 0;
	if(sendbufglag)
	{
		psendbufpart[pGetBufIndex++]= Usart1_data_write();
		Uart_RE_Handle();
	}
	else
	{
		if((Laser_cmd_req  & UART1_RECV_REQ)==0)
		{
			if(index < 20)	
			{
				UartRx_buff[index++] = Usart1_data_write();
				if(index == 20)
				{
					index = 0;
					if((Uart_Comm_Check(UartRx_buff[0])==TRUE)&&(UartRx_buff[19] == Com_checkSum(UartRx_buff)))
						Laser_cmd_req |=UART1_RECV_REQ;
					Uart_RE_Handle();
				}
			}	
		}

	}

}

void checkAppSw(void)
{
    uint32_t app_crc=0;
    uint32_t app_len=0;
    uint32_t app_finish=FLASH_ReadByte(APP_FINISH_ADDR);
    app_crc=FLASH_ReadByte(APP_CRC_ADDR+3);
    app_crc=(app_crc<<8)+FLASH_ReadByte(APP_CRC_ADDR+2);
    app_crc=(app_crc<<8)+FLASH_ReadByte(APP_CRC_ADDR+1);
    app_crc=(app_crc<<8)+FLASH_ReadByte(APP_CRC_ADDR+0);

    app_len=FLASH_ReadByte(APP_LEN_ADDR+3);
    app_len=(app_len<<8)+FLASH_ReadByte(APP_LEN_ADDR+2);
    app_len=(app_len<<8)+FLASH_ReadByte(APP_LEN_ADDR+1);
    app_len=(app_len<<8)+FLASH_ReadByte(APP_LEN_ADDR+0);

    app_finish=FLASH_ReadByte(APP_FINISH_ADDR+3);
    app_finish=(app_finish<<8)+FLASH_ReadByte(APP_FINISH_ADDR+2);
    app_finish=(app_finish<<8)+FLASH_ReadByte(APP_FINISH_ADDR+1);
    app_finish=(app_finish<<8)+FLASH_ReadByte(APP_FINISH_ADDR+0);		
    crc11 = app_crc;
    crc22 = GetCRC32_2(APP_ADDR,app_len);
    // printf("crc=0x%x len=0x%x finish=0x%x\n",app_crc,app_len,app_finish);
    //1. check finish flag
    if(app_finish==APP_FINISH_FLAG)
    {
        //2.0 check crc
        if(crc11 == crc22)
        {
			asm("LDW X,  SP ");
			asm("LD  A,  $FF");
			asm("LD  XL, A  ");
			asm("LDW SP, X  ");    
			asm("JPF $C100");   //goto  app code
     	}
    }    
}
void updateSw_Part0(void)
{
	uint32_t i;
	FLASH_Unlock(FLASH_MemType_Program); 
	for(i = APP_CRC_ADDR;i< APP_ADDR;i++)
	{
    	FLASH_EraseByte(i);
	}
}

void updateSw_Part1(void)
{
	uint32_t i;
	FLASH_Unlock(FLASH_MemType_Program); 
	for(i = (APP_ADDR + pBufPartIndex);i< (APP_ADDR + pBufPartIndex + pBufPartSize);i++)
	{
    	FLASH_EraseByte(i);
	}
}
void updateSw_Part2(uint32_t Address, uint8_t* buf , uint32_t len)
{
    uint32_t size=len;
  //  uint8_t times=10;
	do
    {
     //   times=10;
      //  do
      //  {
        	FLASH_ProgramByte(Address+size-len, buf[size-len]);
        //	delayms(1);
     //   }
    //	while(times--);
	}while(--len);
}

void updateSw_Part2_Block(uint8_t* buf,uint32_t len)
{
	FLASH_Unlock(FLASH_MemType_Program); 
	uint32_t index = 0;
	do
	{
		Flash_WriteDataBlock(paste_start_block_APP +((pBufPartIndex+index) /128),(uint8_t *)(buf+index));
		index += PER_FLASHBLOCK_BYTE;	
		if( len >= 1 && len<PER_FLASHBLOCK_BYTE)
		{
			Flash_WriteDataBlock(paste_start_block_APP +((pBufPartIndex+index) /128),(uint8_t *)(buf+index));
			break;			
		}
	}while((len -= PER_FLASHBLOCK_BYTE)>0);

}
void updateSw_Part3(uint32_t len)
{
    uint8_t buf[4];
	  FLASH_Unlock(FLASH_MemType_Program);
    delayms(2);

    buf[0]=file_crc&0xff;
    buf[1]=(file_crc>>8)&0xff;
    buf[2]=(file_crc>>16)&0xff;
    buf[3]=(file_crc>>24)&0xff;
    updateSw_Part2(APP_CRC_ADDR,buf,4);
    
    buf[0]=len&0xff;
    buf[1]=(len>>8)&0xff;
    buf[2]=(len>>16)&0xff;
    buf[3]=(len>>24)&0xff;
    updateSw_Part2(APP_LEN_ADDR,buf,4);

    buf[0]=APP_FINISH_FLAG&0xff;
    buf[1]=(APP_FINISH_FLAG>>8)&0xff;
    buf[2]=(APP_FINISH_FLAG>>16)&0xff;
    buf[3]=(APP_FINISH_FLAG>>24)&0xff;
    updateSw_Part2(APP_FINISH_ADDR,buf,4);

    delayms(2);
	FLASH_Lock(FLASH_MemType_Program); 
}

#endif	
/**
  * @brief  Sets the fixed programming time
  * @param  FLASH_ProgTime : Indicates the programming time to be fixed
  *          This parameter can be one of the following values:
  *            @arg FLASH_ProgramTime_Standard: Standard programming time fixed at 1/2 tprog
  *            @arg FLASH_ProgramTime_TProg: Programming time fixed at tprog 
  * @retval None
  */
void FLASH_SetProgrammingTime(FLASH_ProgramTime_TypeDef FLASH_ProgTime)
{
  /* Check parameter */
  assert_param(IS_FLASH_PROGRAM_TIME(FLASH_ProgTime));

  FLASH->CR1 &= (uint8_t)(~FLASH_CR1_FIX);
  FLASH->CR1 |= (uint8_t)FLASH_ProgTime;
}

/**
  * @brief  Returns the fixed programming time
  * @param  None
  * @retval Fixed programming time value
  */
FLASH_ProgramTime_TypeDef FLASH_GetProgrammingTime(void)
{
  return((FLASH_ProgramTime_TypeDef)(FLASH->CR1 & FLASH_CR1_FIX));
}

/**
  * @brief  Configures the power state for Flash program and data EEPROM during
  *          wait for interrupt mode
  * @param  FLASH_Power: The power state for Flash program and data EEPROM during
  *         wait for interrupt mode
  *          This parameter can be one of the following values:
  *            @arg FLASH_Power_IDDQ: Flash program and data EEPROM in IDDQ
  *            @arg FLASH_Power_On: Flash program and data EEPROM not in IDDQ 
  * @retval None
  */
void FLASH_PowerWaitModeConfig(FLASH_Power_TypeDef FLASH_Power)
{
  /* Check parameter */
  assert_param(IS_FLASH_POWER(FLASH_Power));

  /* Flash program and data EEPROM in IDDQ during wait for interrupt mode*/
  if (FLASH_Power != FLASH_Power_On)
  {
    FLASH->CR1 |= (uint8_t)FLASH_CR1_WAITM;
  }
  /* Flash program and data EEPROM not in IDDQ during wait for interrupt mode*/
  else
  {
    FLASH->CR1 &= (uint8_t)(~FLASH_CR1_WAITM);
  }
}

/**
  * @}
  */

/** @defgroup FLASH_Group2 FLASH Memory Programming functions
 *  @brief   FLASH Memory Programming functions
 *
@verbatim   
 ===============================================================================
                      FLASH Memory Programming functions
 ===============================================================================  

   The FLASH Memory Programming functions, includes the following functions:
    - void FLASH_DeInit(void);
    - void FLASH_Unlock(FLASH_MemType_TypeDef FLASH_MemType);
    - void FLASH_Lock(FLASH_MemType_TypeDef FLASH_MemType);
    - void FLASH_ProgramByte(uint32_t Address, uint8_t Data);
    - void FLASH_EraseByte(uint32_t Address);
    - void FLASH_ProgramWord(uint32_t Address, uint32_t Data);
    - uint8_t FLASH_ReadByte(uint32_t Address);
   
   Any operation of erase or program should follow these steps:

   1. Call the FLASH_Unlock(FLASH_MemType) function to enable the memory access

   2. Call the desired function to erase or program data

   3. Call the FLASH_Lock() function to disable the memory access 
     (it is recommended to protect the FLASH memory against possible unwanted operation)

@endverbatim
  * @{
  */
  
/**
  * @brief  Deinitializes the FLASH registers to their default reset values.
  * @param  None
  * @retval None
  */
void FLASH_DeInit(void)
{
  FLASH->CR1 = FLASH_CR1_RESET_VALUE;
  FLASH->CR2 = FLASH_CR2_RESET_VALUE;
  FLASH->IAPSR = FLASH_IAPSR_RESET_VALUE;
  (void) FLASH->IAPSR; /* Reading of this register causes the clearing of status flags */
}
  
/**
  * @brief  Unlocks the program or data EEPROM memory
  * @param  FLASH_MemType : Memory type to unlock
  *          This parameter can be one of the following values:
  *            @arg FLASH_MemType_Program: Program memory
  *            @arg FLASH_MemType_Data: Data EEPROM memory 
  * @retval None
  */
void FLASH_Unlock(FLASH_MemType_TypeDef FLASH_MemType)
{
  /* Check parameter */
  assert_param(IS_FLASH_MEMORY_TYPE(FLASH_MemType));

  /* Unlock program memory */
  if (FLASH_MemType == FLASH_MemType_Program)
  {
    FLASH->PUKR = FLASH_RASS_KEY1;
    FLASH->PUKR = FLASH_RASS_KEY2;
  }

  /* Unlock data memory */
  if (FLASH_MemType == FLASH_MemType_Data)
  {
    FLASH->DUKR = FLASH_RASS_KEY2; /* Warning: keys are reversed on data memory !!! */
    FLASH->DUKR = FLASH_RASS_KEY1;
  }
}

/**
  * @brief  Locks the program or data EEPROM memory
  * @param  FLASH_MemType : Memory type
  *          This parameter can be one of the following values:
  *            @arg FLASH_MemType_Program: Program memory
  *            @arg FLASH_MemType_Data: Data EEPROM memory 
  * @retval None
  */
void FLASH_Lock(FLASH_MemType_TypeDef FLASH_MemType)
{
  /* Check parameter */
  assert_param(IS_FLASH_MEMORY_TYPE(FLASH_MemType));
  /* Lock memory */
  FLASH->IAPSR &= (uint8_t)FLASH_MemType;
}

/**
  * @brief  Programs one byte in program or data EEPROM memory
  * @param  Address : Address where the byte will be programmed
  * @param  Data : Value to be programmed
  * @retval None
  */
void FLASH_ProgramByte(uint32_t Address, uint8_t Data)
{
  /* Check parameters */
  assert_param(IS_FLASH_ADDRESS(Address));

  *(PointerAttr uint8_t*) (MemoryAddressCast)Address = Data;
}

/**
  * @brief  Erases one byte in the program or data EEPROM memory
  * @param  Address : Address of the byte to erase
  * @retval None
  */
void FLASH_EraseByte(uint32_t Address)
{
  /* Check parameter */
  assert_param(IS_FLASH_ADDRESS(Address));

  *(PointerAttr uint8_t*) (MemoryAddressCast)Address = FLASH_CLEAR_BYTE; /* Erase byte */
}
void Flash_EraseFlashBlock(uint32_t addr,uint32_t len)
{
	uint16_t cnt;
	uint8_t data[128]={0x00};
	len=(len+127)/128;
	FLASH_Unlock(FLASH_MemType_Program);
	while (FLASH_GetFlagStatus(FLASH_FLAG_HVOFF) == RESET);

	for(cnt = 0;cnt <len;cnt++)
	{
		FLASH_ProgramBlock(cnt+130+(addr/128), FLASH_MemType_Program, FLASH_ProgramMode_Standard,data);
	}
	
	while (FLASH_GetFlagStatus(FLASH_FLAG_HVOFF) == RESET);
	FLASH_Lock(FLASH_MemType_Program);

}
void Flash_WriteDataBlock(uint16_t block_count, uint8_t *Buffer)
{
	FLASH_Unlock(FLASH_MemType_Program);
	while (FLASH_GetFlagStatus(FLASH_FLAG_HVOFF) == RESET);
	FLASH_ProgramBlock(block_count, FLASH_MemType_Program, FLASH_ProgramMode_Standard,Buffer);
	while (FLASH_GetFlagStatus(FLASH_FLAG_HVOFF) == RESET);
	FLASH_Lock(FLASH_MemType_Program);
}
/**
  * @brief  Programs one word (4 bytes) in program or data EEPROM memory
  * @param  Address : The address where the data will be programmed
  * @param  Data : Value to be programmed
  * @retval None
  */
void FLASH_ProgramWord(uint32_t Address, uint32_t Data)
{
  /* Check parameters */
  assert_param(IS_FLASH_ADDRESS(Address));
  /* Enable Word Write Once */
  FLASH->CR2 |= FLASH_CR2_WPRG;

  /* Write one byte - from lowest address*/
  *((PointerAttr uint8_t*)(MemoryAddressCast)Address)       = *((uint8_t*)(&Data));   
  /* Write one byte*/
  *(((PointerAttr uint8_t*)(MemoryAddressCast)Address) + 1) = *((uint8_t*)(&Data) + 1);
  /* Write one byte*/
  *(((PointerAttr uint8_t*)(MemoryAddressCast)Address) + 2) = *((uint8_t*)(&Data) + 2); 
  /* Write one byte - from higher address*/
  *(((PointerAttr uint8_t*)(MemoryAddressCast)Address) + 3) = *((uint8_t*)(&Data) + 3); 
}

/**
  * @brief  Reads one byte from flash memory
  * @param  Address : Address to read
  * @retval Value of the byte
  */
uint8_t FLASH_ReadByte(uint32_t Address)
{
  /* Read byte */
  return(*(PointerAttr uint8_t *) (MemoryAddressCast)Address);
}
/**
  * @}
  */

/** @defgroup FLASH_Group3 Option Bytes Programming functions
 *  @brief   Option Bytes Programming functions 
 *
@verbatim   
 ===============================================================================
                        Option Bytes Programming functions
 ===============================================================================  

   The FLASH_Option Bytes Programming_functions, includes the following functions:

   - void FLASH_ProgramOptionByte(uint16_t Address, uint8_t Data);
   - void FLASH_EraseOptionByte(uint16_t Address);
   - FunctionalState FLASH_GetReadOutProtectionStatus(void);
   - uint16_t FLASH_GetBootSize(void);
   - uint16_t FLASH_GetCodeSize(void);
   
   Any operation of erase or program should follow these steps:
   
   1. Call the FLASH_Unlock(FLASH_MemType_Data); function to enable the Flash 
      option control register access
   
   2. Call the desired function to erase or program data
      - void FLASH_ProgramOptionByte(uint16_t Address, uint8_t Data); => to program
        the option byte Address with the desired Data value.  
      - void FLASH_EraseOptionByte(uint16_t Address); => to erase the option byte
        Address. 			 
   
   3. Once all needed option bytes to be programmed are correctly written, call the
      FLASH_Lock(FLASH_MemType_Data) to disable the memory access ( It is recommended to
      protect the FLASH memory against possible unwanted operation)

@endverbatim
  * @{
  */
  
/**
  * @brief  Programs option byte
  * @param  Address : option byte address to program
  * @param  Data : Value to write
  * @retval None
  */
void FLASH_ProgramOptionByte(uint16_t Address, uint8_t Data)
{
  /* Check parameter */
  assert_param(IS_OPTION_BYTE_ADDRESS(Address));

  /* Enable write access to option bytes */
  FLASH->CR2 |= FLASH_CR2_OPT;

  /* Program option byte and his complement */
  *((PointerAttr uint8_t*)Address) = Data;

  FLASH_WaitForLastOperation(FLASH_MemType_Program);

  /* Disable write access to option bytes */
  FLASH->CR2 &= (uint8_t)(~FLASH_CR2_OPT);
}

/**
  * @brief  Erases option byte
  * @param  Address : Option byte address to erase
  * @retval None
  */
void FLASH_EraseOptionByte(uint16_t Address)
{
  /* Check parameter */
  assert_param(IS_OPTION_BYTE_ADDRESS(Address));

  /* Enable write access to option bytes */
  FLASH->CR2 |= FLASH_CR2_OPT;

  /* Erase option byte and his complement */
  *((PointerAttr uint8_t*)Address) = FLASH_CLEAR_BYTE;

  FLASH_WaitForLastOperation(FLASH_MemType_Program);

  /* Disable write access to option bytes */
  FLASH->CR2 &= (uint8_t)(~FLASH_CR2_OPT);
}

/**
  * @brief  Returns the FLASH Read Out Protection Status.
  * @param  None
  * @retval FLASH Read Out Protection Status.
  *         This parameter can be a ENABLE or DISABLE
  */
FunctionalState FLASH_GetReadOutProtectionStatus(void)
{
  FunctionalState state = DISABLE;

  if (OPT->ROP == FLASH_READOUTPROTECTION_KEY)
  {
    /* The status of the Flash read out protection is enabled*/
    state =  ENABLE;
  }
  else
  {
    /* The status of the Flash read out protection is disabled*/
    state =  DISABLE;
  }

  return state;
}

/**
  * @brief  Returns the Boot memory size in bytes
  * @param  None
  * @retval Boot memory size in bytes
  */
uint16_t FLASH_GetBootSize(void)
{
  uint16_t temp = 0;

  /* Calculates the number of bytes */
  temp = (uint16_t)((uint16_t)OPT->UBC * (uint16_t)128);

  /* Correction because size upper 8kb doesn't exist */
  if (OPT->UBC > 0x7F)
  {
    temp = 8192;
  }

  /* Return value */
  return(temp);

}

/**
 *
  * @brief  Returns the Code Area size in bytes
  * @param  None
  * @retval Code Area size in bytes
  */
uint16_t FLASH_GetCodeSize(void)
{
  uint16_t temp = 0;

  /* Calculates the number of bytes */
  temp = (uint16_t)((uint16_t)OPT->PCODESIZE * (uint16_t)128);

  /* Correction because size upper of 8kb doesn't exist */
  if (OPT->PCODESIZE > 0x7F)
  {
    temp = 8192;
  }

  /* Return value */
  return(temp);
}

/**
  * @}
  */

/** @defgroup FLASH_Group4 Interrupts and flags management functions
 *  @brief   Interrupts and flags management functions
 *
@verbatim   
 ===============================================================================
                  Interrupts and flags management functions
 ===============================================================================  

@endverbatim
  * @{
  */
  
/**
  * @brief  Enables or Disables the Flash interrupt mode
  * @param  NewState : The new state of the flash interrupt mode
    *         This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void FLASH_ITConfig(FunctionalState NewState)
{

  /* Check parameter */
  assert_param(IS_FUNCTIONAL_STATE(NewState));

  if (NewState != DISABLE)
  {
    /* Enables the interrupt sources */
    FLASH->CR1 |= FLASH_CR1_IE;
  }
  else
  {
    /* Disables the interrupt sources */
    FLASH->CR1 &= (uint8_t)(~FLASH_CR1_IE);
  }
}

/**
  * @brief  Checks whether the specified FLASH flag is set or not.
  * @param  FLASH_FLAG : specifies the Flash Flag to check.
  *          This parameter can be one of the following values:
  *            @arg FLASH_FLAG_HVOFF: End of high voltage
  *            @arg FLASH_FLAG_DUL: Data EEPROM unlocked
  *            @arg FLASH_FLAG_EOP: End of programming (write or erase operation)
  *            @arg FLASH_FLAG_PUL: Flash Program memory unlocked
  *            @arg FLASH_FLAG_WR_PG_DIS: Write attempted to protected page       
  * @retval Indicates the state of the Flash_FLAG.
  *         This parameter can be SET or RESET
  */
FlagStatus FLASH_GetFlagStatus(FLASH_FLAG_TypeDef FLASH_FLAG)
{
  FlagStatus status = RESET;
  assert_param(IS_FLASH_FLAGS(FLASH_FLAG));

  /* Check the status of the specified flash flag*/
  if ((FLASH->IAPSR  & (uint8_t)FLASH_FLAG) != (uint8_t)RESET)
  {
    status = SET; /* Flash_FLAG is set*/
  }
  else
  {
    status = RESET; /* Flash_FLAG is reset*/
  }

  /* Return the Flash_FLAG status*/
  return status;
}

/**
  * @}
  */ 
  
/** @defgroup FLASH_Group5 Functions to be executed from RAM
 *  @brief  Functions to be executed from RAM
 *
@verbatim   
 ===============================================================================
                         Functions to be executed from RAM
 ===============================================================================  
 
 All the functions defined below must be executed from RAM exclusively, except
 for the FLASH_WaitForLastOperation function which can be executed from Flash.

 Steps of the execution from RAM differs from one toolchain to another:
 - For Cosmic Compiler:
    1- Define a segment FLASH_CODE by the mean of " #pragma section (FLASH_CODE)".
    This segment is defined in the stm8l15x_flash.c file.
  2- Uncomment the "#define RAM_EXECUTION  (1)" line in the stm8l15x.h file,
    or define it in Cosmic compiler preprocessor to enable the FLASH_CODE segment
   definition.
  3- In STVD Select Project\Settings\Linker\Category "input" and in the RAM section
    add the FLASH_CODE segment with "-ic" options.
  4- In main.c file call the _fctcpy() function with first segment character as 
    parameter "_fctcpy('F');" to load the declared moveable code segment
    (FLASH_CODE) in RAM before execution.
  5- By default the _fctcpy function is packaged in the Cosmic machine library,
    so the function prototype "int _fctcopy(char name);" must be added in main.c
    file.

  - For Raisonance Compiler
   1- Use the inram keyword in the function declaration to specify that it can be
    executed from RAM.
    This is done within the stm8l15x_flash.c file, and it's conditioned by 
    RAM_EXECUTION definition.
   2- Uncomment the "#define RAM_EXECUTION  (1)" line in the stm8l15x.h file, or 
   define it in Raisonance compiler preprocessor to enable the access for the 
   inram functions.
   3- An inram function code is copied from Flash to RAM by the C startup code. 
   In some applications, the RAM area where the code was initially stored may be
   erased or corrupted, so it may be desirable to perform the copy again. 
   Depending on the application memory model, the memcpy() or fmemcpy() functions
   should be used to perform the copy.
      � In case your project uses the SMALL memory model (code smaller than 64K),
       memcpy()function is recommended to perform the copy
      � In case your project uses the LARGE memory model, functions can be 
      everywhere in the 24-bits address space (not limited to the first 64KB of
      code), In this case, the use of memcpy() function will not be appropriate,
      you need to use the specific fmemcpy() function (which copies objects with
      24-bit addresses).
      - The linker automatically defines 2 symbols for each inram function:
           � __address__functionname is a symbol that holds the Flash address 
           where the given function code is stored.
           � __size__functionname is a symbol that holds the function size in bytes.
     And we already have the function address (which is itself a pointer)
  4- In main.c file these two steps should be performed for each inram function:
     � Import the "__address__functionname" and "__size__functionname" symbols
       as global variables:
         extern int __address__functionname; // Symbol holding the flash address
         extern int __size__functionname;    // Symbol holding the function size
     � In case of SMALL memory model use, Call the memcpy() function to copy the
      inram function to the RAM destination address:
                memcpy(functionname, // RAM destination address
                      (void*)&__address__functionname, // Flash source address
                      (int)&__size__functionname); // Code size of the function
     � In case of LARGE memory model use, call the fmemcpy() function to copy 
     the inram function to the RAM destination address:
                 memcpy(functionname, // RAM destination address
                      (void @far*)&__address__functionname, // Flash source address
                      (int)&__size__functionname); // Code size of the function

 - For IAR Compiler:
    1- Use the __ramfunc keyword in the function declaration to specify that it 
    can be executed from RAM.
    This is done within the stm8l15x_flash.c file, and it's conditioned by 
    RAM_EXECUTION definition.
    2- Uncomment the "#define RAM_EXECUTION  (1)" line in the stm8l15x.h file, or 
   define it in IAR compiler preprocessor to enable the access for the 
   __ramfunc functions.
 
 The Flash_DataProgram example provided within the STM8L15x_StdPeriph_Lib package
 details all the steps described above.

@endverbatim
  * @{
  */
   
/**
  * @brief
  *******************************************************************************
  *                         Execution from RAM enable
  *******************************************************************************
  *
  * To enable execution from RAM you can either uncomment the following define 
  * in the stm8l15x.h file or define it in your toolchain compiler preprocessor
  * - #define RAM_EXECUTION  (1) 
  */

#if defined (_COSMIC_) && defined (RAM_EXECUTION)
 #pragma section (FLASH_CODE)
#endif  /* _COSMIC_ && RAM_EXECUTION */

/**
  * @brief  Configures the power state for Flash program and data EEPROM during
  *         run, low power run and low power wait modes
  * @note   This function must be called and executed from RAM.
  * @param  FLASH_Power: power state of the Flash program and data EEPROM
  *          This parameter can be one of the following values:
  *            @arg FLASH_Power_IDDQ: Flash program and data EEPROM in IDDQ
  *            @arg FLASH_Power_On: Flash program and data EEPROM not in IDDQ 
  * @retval None
  */
IN_RAM(void FLASH_PowerRunModeConfig(FLASH_Power_TypeDef FLASH_Power))
{
  /* Check parameter */
  assert_param(IS_FLASH_POWER(FLASH_Power));

  if (FLASH_Power != FLASH_Power_On)
  {
  FLASH->CR1 |= (uint8_t)FLASH_CR1_EEPM;
  }
  else
  {
    FLASH->CR1 &= (uint8_t)(~FLASH_CR1_EEPM);
  }
}

/**
  * @brief  Checks the power status for Flash program and data EEPROM
  * @note   This function should be called and executed from RAM.
  * @param  None
  * @retval Flash program and data EEPROM power status
  *         The returned value can be one of the following:
  *         - FLASH_PowerStatus_IDDQDuringWaitMode: IDDQ during Wait mode
  *         - FLASH_PowerStatus_IDDQDuringRunMode: IDDQ during Run mode
  *         - FLASH_PowerStatus_IDDQDuringWaitAndRunModes: IDDQ during Wait/Run mode
  *         - FLASH_PowerStatus_On: Powered on during Wait and Run modes
  */
IN_RAM(FLASH_PowerStatus_TypeDef FLASH_GetPowerStatus(void))
{
  return((FLASH_PowerStatus_TypeDef)(FLASH->CR1 & (uint8_t)0x0C));
}

/**
  * @brief  Programs a memory block
  * @note   This function should be called and executed from RAM.
  * @param  FLASH_MemType : The type of memory to program
  *          This parameter can be one of the following values:
  *            @arg FLASH_MemType_Program: Program memory
  *            @arg FLASH_MemType_Data: Data EEPROM memory 
  * @param  BlockNum : The block number
  * @param  FLASH_ProgMode : The programming mode.
  *          This parameter can be one of the following values:
  *            @arg FLASH_ProgramMode_Standard: Standard programming mode
  *            @arg FLASH_ProgramMode_Fast: Fast programming mode
  * @param  Buffer : Pointer to buffer containing source data.
  * @retval None.
  */
IN_RAM(void FLASH_ProgramBlock(uint16_t BlockNum, FLASH_MemType_TypeDef FLASH_MemType,
                        FLASH_ProgramMode_TypeDef FLASH_ProgMode, uint8_t *Buffer))
{
  uint16_t Count = 0;
  uint32_t startaddress = 0;

  /* Check parameters */
  assert_param(IS_FLASH_MEMORY_TYPE(FLASH_MemType));
  assert_param(IS_FLASH_PROGRAM_MODE(FLASH_ProgMode));
  if (FLASH_MemType == FLASH_MemType_Program)
  {
  assert_param(IS_FLASH_PROGRAM_BLOCK_NUMBER(BlockNum));
    startaddress = FLASH_PROGRAM_START_PHYSICAL_ADDRESS;
  }
  else
  {
    assert_param(IS_FLASH_DATA_EEPROM_BLOCK_NUMBER(BlockNum));
    startaddress = FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;
  }

  /* Point to the first block address */
  startaddress = startaddress + ((uint32_t)BlockNum * FLASH_BLOCK_SIZE);

  /* Selection of Standard or Fast programming mode */
  if (FLASH_ProgMode == FLASH_ProgramMode_Standard)
  {
  /* Standard programming mode */
  FLASH->CR2 |= FLASH_CR2_PRG;
  }
  else
  {
  /* Fast programming mode */
  FLASH->CR2 |= FLASH_CR2_FPRG;
  }

  /* Copy data bytes from RAM to FLASH memory */
  for (Count = 0; Count < FLASH_BLOCK_SIZE; Count++)
  {
    *((PointerAttr uint8_t*) (MemoryAddressCast)startaddress + Count) = ((uint8_t)(Buffer[Count]));
  }
}

/**
  * @brief  Erases a block in the program or data memory.
  * @note   This function should be called and executed from RAM.
  * @param  BlockNum : Indicates the block number to erase
  * @param  FLASH_MemType :  The type of memory to erase
  *          This parameter can be one of the following values:
  *            @arg FLASH_MemType_Program: Program memory
  *            @arg FLASH_MemType_Data: Data EEPROM memory 
  * @retval None.
  */
IN_RAM(void FLASH_EraseBlock(uint16_t BlockNum, FLASH_MemType_TypeDef FLASH_MemType))
{
  uint32_t startaddress = 0;
#if defined (STM8L15X_MD) || defined (STM8L15X_MDP) || defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
  uint32_t PointerAttr  *pwFlash;
  
#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
  uint8_t PointerAttr  *pwFlash;
#endif

  /* Check parameters */
  assert_param(IS_FLASH_MEMORY_TYPE(FLASH_MemType));
  if (FLASH_MemType == FLASH_MemType_Program)
  {
  assert_param(IS_FLASH_PROGRAM_BLOCK_NUMBER(BlockNum));
    startaddress = FLASH_PROGRAM_START_PHYSICAL_ADDRESS;
  }
  else
  {
    assert_param(IS_FLASH_DATA_EEPROM_BLOCK_NUMBER(BlockNum));
    startaddress = FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;
  }

  /* Point to the first block address */
#if defined (STM8L15X_MD) || defined (STM8L15X_MDP)|| defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
  pwFlash = (PointerAttr uint32_t *)(uint16_t)(startaddress + ((uint32_t)BlockNum * FLASH_BLOCK_SIZE));
  
#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
  pwFlash = (PointerAttr uint8_t *)(uint32_t)(startaddress + ((uint32_t)BlockNum * FLASH_BLOCK_SIZE));
#endif

  /* Enable erase block mode */
  FLASH->CR2 |= FLASH_CR2_ERASE;

#if defined (STM8L15X_MD) || defined (STM8L15X_MDP) || defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
  *pwFlash = (uint32_t)0;  

#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
  *pwFlash = (uint8_t)0;
  *(pwFlash + 1) = (uint8_t)0;
  *(pwFlash + 2) = (uint8_t)0;
  *(pwFlash + 3) = (uint8_t)0;
#endif
}

/**
  * @brief  Waits for a Flash operation to complete.
  * @note   The call and execution of this function must be done from RAM in case
  *         of Block operation, otherwise it can be executed from Flash
  * @param  FLASH_MemType : Memory type
  *          This parameter can be one of the following values:
  *            @arg FLASH_MemType_Program: Program memory
  *            @arg FLASH_MemType_Data: Data EEPROM memory 
  * @retval FLASH status
  */
IN_RAM(FLASH_Status_TypeDef FLASH_WaitForLastOperation(FLASH_MemType_TypeDef FLASH_MemType))
{
  uint32_t timeout = OPERATION_TIMEOUT;
  uint8_t flagstatus = 0x00;
  /* Wait until operation completion or write protected page occurred */
  if (FLASH_MemType == FLASH_MemType_Program)
  {
  while ((flagstatus == 0x00) && (timeout != 0x00))
    {
      flagstatus = (uint8_t)(FLASH->IAPSR & (uint8_t)(FLASH_IAPSR_EOP |
                             FLASH_IAPSR_WR_PG_DIS));
      timeout--;
    }
  }
  else
  {
    while ((flagstatus == 0x00) && (timeout != 0x00))
    {
      flagstatus = (uint8_t)(FLASH->IAPSR & (uint8_t)(FLASH_IAPSR_HVOFF |
                             FLASH_IAPSR_WR_PG_DIS));
      timeout--;
    }
  }
  if (timeout == 0x00 )
  {
  flagstatus = FLASH_Status_TimeOut;
  }

  return((FLASH_Status_TypeDef)flagstatus);
}

#if defined (_COSMIC_) && defined (RAM_EXECUTION)
 /* End of FLASH_CODE section */
 #pragma section ()
#endif /* _COSMIC_ && RAM_EXECUTION */

/**
  * @}
  */

/**
  * @}
  */
   
  /**
  * @}
  */ 
  
  /**
  * @}
  */ 
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
