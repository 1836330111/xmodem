#include "stm8l15x.h"
#include "string.h"

#define TIM2_Period     0xFFFF


#define def_wordlen      USART_WordLength_8b
#define def_stopbit        USART_StopBits_1
#define def_parity         USART_Parity_No
#define UART1_RECV_REQ			0X01
#define UART1_SEND_REQ			0X02
#define UART_BUFF_SIZE			20

#define UPDATE_APP_CMD          0x3A

typedef  enum
{
	TARGET_HOST = 0x0A,  

	TARGET_DP9320,

	TARGET_IDU,

	TARGET_DDP,

	TARGET_DDP2,

	TARGET_STM32,

} COMM_TYPE;


void hardware_init(void);
void delayms(uint32_t n);

void Uart_RE_Handle(void);
uint8_t Uart_Comm_Check(uint8_t u8Head);
uint8_t Com_checkSum(uint8_t* pbuf);

