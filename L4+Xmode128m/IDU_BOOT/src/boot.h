#ifndef _BOOT_H
#define _BOOT_H
#include "stm8l15x.h"
//用户代码的起始地址
#define MAIN_USER_Start_ADDR     ((uint32_t)0xc100)

//定义指向函数的指针类型
typedef void ( *AppMainTyp)(void);


typedef struct
{
 
  uint8_t (*USART1_Receive)(uint8_t *,uint32_t); //接收一个字节数据  附带超时时间 返回0接收到数据    其它未收到数据
  
  void (*USART1_SendByte)(uint8_t);//发送一个字节数据   无返回型
    
  void (*USART1_SendBytes)(uint8_t * ,uint16_t);//发送指定长度数据   
}
USART1_Interface;  //通信接口结构体


//数据通信接口
extern const USART1_Interface BootInterface;

//重新初始化STM8的中断向量表  把它重新定义到APP的中断向量中
void STM8_HanderIqr_Init(void);


uint32_t FLASH_ReadWord(uint32_t Address);

void goto_app(void);


















#endif


