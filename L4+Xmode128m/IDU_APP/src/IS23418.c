
#include "public.h"



uint8_t SPI_SENDREADByte(uint8_t byte)
{
	/* Loop while DR register in not emplty */
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_TXE) == RESET);

	/* Send byte through the SPI peripheral */
	SPI_SendData(LD_SPI, byte);

	/* Wait to receive a byte */
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_RXNE) == RESET);

	/* Return the byte read from the SPI bus */
	return SPI_ReceiveData(LD_SPI);
}





void ISL23425_write(uint8_t ID,uint8_t reg, uint8_t data)
{

	uint8_t send[2] = {0};
    GPIO_ResetBits(GPIOB ,LD_SPI_CS_3);
	send[0] = reg|ID ;//|CMD_REG_WRTE|1;//CMD_ACR_WRTE ;//CMD_REG_WRTE;
	send[1] = data;
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_TXE) == RESET);
	/* Send byte through the SPI peripheral */
	SPI_SendData(LD_SPI, send[0] );
	/* Wait to receive a byte */
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_RXNE) == RESET);
	/* Return the byte read from the SPI bus */
	 SPI_ReceiveData(LD_SPI);
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_TXE) == RESET);
	/* Send byte through the SPI peripheral */
	SPI_SendData(LD_SPI, send[1] );
	/* Wait to receive a byte */
	while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_RXNE) == RESET);
	/* Return the byte read from the SPI bus */
	 SPI_ReceiveData(LD_SPI);

    GPIO_SetBits(GPIOB ,LD_SPI_CS_3);

}
    


void ISL23425_REG_write(uint8_t ID,uint8_t reg, uint8_t data)
{

	uint8_t send[3] = {0};
    //GPIO_ResetBits(GPIOB ,LD_SPI_CS_3);
    GPIOB->ODR &= (uint8_t)(~LD_SPI_CS_3);

    
	send[0] = reg|ID ;//|CMD_REG_WRTE|1;//CMD_ACR_WRTE ;//CMD_REG_WRTE;
	send[1] = data;
	//while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_TXE) == RESET);
    while((SPI1->SR & SPI_FLAG_TXE) == (uint8_t)RESET);
    
	/* Send byte through the SPI peripheral */
   
	//SPI_SendData(LD_SPI, send[0] );
     SPI1->DR = send[0];
	/* Wait to receive a byte */
	//while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_RXNE) == RESET);
    while((SPI1->SR & SPI_FLAG_RXNE) == (uint8_t)RESET);
    
	/* Return the byte read from the SPI bus */
	// SPI_ReceiveData(LD_SPI);
    send[2]=SPI1->DR;

	//while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_TXE) == RESET);
    while((SPI1->SR & SPI_FLAG_TXE) == (uint8_t)RESET);
    
	/* Send byte through the SPI peripheral */
	//SPI_SendData(LD_SPI, send[1] );
    SPI1->DR = send[1];

    
	/* Wait to receive a byte */
	//while (SPI_GetFlagStatus(LD_SPI, SPI_FLAG_RXNE) == RESET);
    while((SPI1->SR & SPI_FLAG_RXNE) == (uint8_t)RESET);
    
	/* Return the byte read from the SPI bus */
	// SPI_ReceiveData(LD_SPI);
    send[2]=SPI1->DR;

    //GPIO_SetBits(GPIOB ,LD_SPI_CS_3);
    GPIOB->ODR |= LD_SPI_CS_3;

}
    



 
void ISL23418_read(uint8_t ID,uint8_t *pData, uint8_t reg)
{
	uint8_t send[4] = {0};

	 GPIO_ResetBits(GPIOB ,LD_SPI_CS_3);
    
	//send[0] = reg | CMD_REG_READ;
	send[0] = reg | CMD_ACR_READ;
	send[1] = 0x00;
	send[2] = CMD_NOP;
	send[3] = 0x00;
    
	SPI_SENDREADByte(send[0]);
	SPI_SENDREADByte(send[1]);
	SPI_SENDREADByte(send[2]);
	*pData = SPI_SENDREADByte(send[3]);
    
	GPIO_SetBits(GPIOB ,LD_SPI_CS_3);
    
}


void ISL23425_up(uint8_t data)
{
	int i;

	for(i=0xf0;i>=data;i--)
	{
        ISL23425_REG_write(0,CMD_REG_WRTE,i);
        ISL23425_REG_write(1,CMD_REG_WRTE,i);
		delayms(2);
	}
}


void ISL23425_down(void)
{
	int i;
	//for(i=0;i<7;i++)
	{
        ISL23425_REG_write(0,CMD_REG_WRTE,i);
        ISL23425_REG_write(1,CMD_REG_WRTE,i);
		delayms(20);
	}
}




#define STEP_ADD 1


void ISL23425_Sine_Loop(uint8_t data)
{
	int i;
	for(i=0xff;i>=data;i=i-STEP_ADD)
	{
       // ISL23425_REG_write(0,CMD_REG_WRTE,i);
      //  ISL23425_REG_write(1,CMD_REG_WRTE,i);

        ISL23425_write(0,CMD_REG_WRTE,i);
        delayms(1);
        ISL23425_write(1,CMD_REG_WRTE,i);

		delayms(1);
	}

    for(i=data;i<=0xff;i=i+STEP_ADD)
    {
        //ISL23425_REG_write(0,CMD_REG_WRTE,i);
       // ISL23425_REG_write(1,CMD_REG_WRTE,i);
        
        ISL23425_write(0,CMD_REG_WRTE,i);
        delayms(1);
        ISL23425_write(1,CMD_REG_WRTE,i);
        delayms(1);
    }



  
}






void ISL23425_data(uint8_t reg, uint8_t data)
{

    if(data<0x30)
        {
            data=0x30;
        }
  
    //ISL23425_REG_write(0,reg,data);
    //ISL23425_REG_write(1,reg,data);

    ISL23425_REG_write(0,reg,data);
    ISL23425_REG_write(1,reg,data);

}





void ISL23418_up(uint8_t data)
{
	int i;

		for(i=0xf0;i>=data;i--)
		{
            ISL23418_write(0,CMD_REG_WRTE,0xff);
            ISL23418_write(1,CMD_REG_WRTE,0xff);
			delayms(100);
		}
}


void ISL23418_down(void)
{
	int i;
	for(i=0;i<7;i++)
	{
		ISL23418_write(1,REG_WR,70-5*i);
		ISL23418_write(2,REG_WR,70-5*i);
		ISL23418_write(3,REG_WR,70-5*i);
		ISL23418_write(4,REG_WR,70-5*i);
		
		delayms(20);
	}
}







void SPI_INIT(void)
{
	/* Enable SPI clock */
	CLK_PeripheralClockConfig(LD_SPI_CLK, ENABLE);


	/* Set the MOSI,MISO and SCK at high level */
	GPIO_ExternalPullUpConfig(LD_SPI_PIN,  LD_SPI_MISO_PIN | LD_SPI_MOSI_PIN | \
	            LD_SPI_SCK_PIN, ENABLE);


	GPIO_SetBits(GPIOF ,LD_SPI_CS_1);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_2);
	GPIO_SetBits(GPIOB ,LD_SPI_CS_3);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_4);
	/* SD_SPI Config */
	SPI_Init(LD_SPI, SPI_FirstBit_MSB, IS234XX_SPI_SPEED, SPI_Mode_Master,
	SPI_CPOL_Low, SPI_CPHA_2Edge, SPI_Direction_2Lines_FullDuplex,
	SPI_NSS_Soft, 0x07);



	/* SD_SPI enable */
	SPI_Cmd(LD_SPI, ENABLE);

  
}












