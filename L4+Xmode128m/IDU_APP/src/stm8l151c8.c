#include "public.h"


#include "stm8l151c8.h"

#include "stm8l15x_clk.h"



//uint8_t UartRx_buff[20];
uint8_t Do_Recv_Cmd[20];  



void Clk_Init(void)
{
	CLK_DeInit();
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
	CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
	while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI);
	CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
	//CLK_PeripheralClockConfig(CLK_Peripheral_USART2, ENABLE);
	//CLK_PeripheralClockConfig(CLK_Peripheral_USART3, ENABLE);

	CLK_PeripheralClockConfig(CLK_Peripheral_DAC , ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1 , ENABLE);
	
	CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_COMP, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM1, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
    CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM5, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);
	
   // CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
	
}








void Gpio_Init(void)
{
	//GPIO_Init(GPIOC , GPIO_Pin_0|GPIO_Pin_1 , GPIO_Mode_In_PU_No_IT);     //sda,scl 
	//GPIO_Init(GPIOC , GPIO_Pin_0|GPIO_Pin_1 , GPIO_Mode_Out_PP_High_Fast);     //sda,scl 

	GPIO_Init(GPIOD , GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);// good   pg_ok  //0:ng   1 :OK
	GPIO_Init(GPIOD , GPIO_Pin_3, GPIO_Mode_In_FL_No_IT);// LD_EN   DLP TO IDU
	GPIO_Init(GPIOB , GPIO_Pin_4, GPIO_Mode_In_FL_No_IT);// cw index


	GPIO_Init(GPIOD,GPIO_Pin_5,GPIO_Mode_Out_PP_High_Fast);     //SPOKE OUT
	GPIO_Init(GPIOD,GPIO_Pin_7,GPIO_Mode_In_FL_No_IT);         //SPOKE OUT   FUCK

	//GPIO_Init(GPIOE , GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5,GPIO_Mode_In_FL_No_IT);     //RGB in
	GPIO_Init(GPIOD , GPIO_Pin_2,GPIO_Mode_Out_PP_High_Fast);//LARSE  ON/OFF

	GPIO_Init(GPIOB , GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);    //CS1
	GPIO_Init(GPIOF , GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);    //CS2
	GPIO_Init(GPIOD , GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast);    //CS3
	GPIO_Init(GPIOD , GPIO_Pin_4, GPIO_Mode_Out_PP_High_Fast);    //CS4 

    GPIO_Init(GPIOB , GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);// CS 

    GPIO_Init(GPIOA , GPIO_Pin_3, GPIO_Mode_Out_PP_Low_Fast);//  LED  RED
    
	disableInterrupts();
#if 1
    //B
    GPIO_Init(GPIOE , GPIO_Pin_3,GPIO_Mode_In_FL_IT); //Input pull-up, external interrupt
    // EXTI_DeInit (); //恢复中断的所有设置
    //EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Rising);//下降沿触发
    EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling);
    ITC_SetSoftwarePriority(EXTI3_IRQn, ITC_PriorityLevel_3);//设置中断优先级 

   //G
   
   GPIO_Init(GPIOE , GPIO_Pin_4,GPIO_Mode_In_FL_IT); //Input pull-up, external interrupt
   //EXTI_SetPinSensitivity(EXTI_Pin_4, EXTI_Trigger_Falling);//下降沿触发
   EXTI_SetPinSensitivity(EXTI_Pin_4, EXTI_Trigger_Falling);
   ITC_SetSoftwarePriority(EXTI4_IRQn, ITC_PriorityLevel_3);//设置中断优先级 


   //R
    GPIO_Init(GPIOE , GPIO_Pin_5,GPIO_Mode_In_FL_IT); //Input pull-up, external interrupt
   //EXTI_SetPinSensitivity(EXTI_Pin_5, EXTI_Trigger_Rising_Falling);//上下降沿触发
   EXTI_SetPinSensitivity(EXTI_Pin_5, EXTI_Trigger_Falling);//上下降沿触发
   ITC_SetSoftwarePriority(EXTI5_IRQn, ITC_PriorityLevel_3);//设置中断优先级 
#endif



   // GPIO_Init(GPIOD , GPIO_Pin_0, GPIO_Mode_Out_PP_Low_Fast);    //fan pwm

  
}



//麦格米特   和力维
//maige mite 拉高  PB1  PB2




uint8_t g_device_version = 0;  // 0x00  力维    ;    0x01  麦格米特


void Get_Device_Version(void)
{

//  PB1  PB2 检测电源是哪家的
//
//  厂家      PB1      PB2
// MAIGE          H         H
// 力维        L         L
//
//
GPIO_Init(GPIOB , GPIO_Pin_1|GPIO_Pin_2, GPIO_Mode_In_FL_No_IT);




/*if(((GPIOB->IDR&GPIO_Pin_1)==GPIO_Pin_1)&&\
    ((GPIOB->IDR&GPIO_Pin_2)==GPIO_Pin_2))
{
    g_device_version = 1;
}
else
{
    g_device_version = 0;
}*/
 g_device_version = 0;


}













void Hardware_Init(void)
{	
    SYSCFG_RIDeInit();
    disableInterrupts();
    Clk_Init();
    Gpio_Init();
	//Adc_DMA_Config();
  	DMA_Config_Uart();

  //  User_Tim1_Init_For_Delay();
   // User_Tim1_Init();
   
    User_Tim1_Init_For_Dynamic_contrast();
  
    //User_Tim1_Init_For_DIFF_RG_Y();  //for diff  R G, AND Y DLP'S  GPIO  if  < x ,is y
    User_Tim2_Init();  //for delay
   // User_Tim4_Init_For_Delay();
    //User_Tim4_Init();

    delayms(10);
    //Get_Device_Version();

    
    //User_Tim4_For_Uart_Timeout();
	User_Tim3_For_Y_SYNC();

    //User_TIM3_For_PWM();
    User_Tim5_Init_For_Spoke();


    User_Uart1_Int(115200);
    //User_Uart2_Int(B115200);
    //User_Uart3_Int(B115200);
    //IIC_Init();
    
    Mcp4131_SPI_INIT();

    //Adc_Config();

}






