
#ifndef __POWER_MANAGE_H__
#define __POWER_MANAGE_H__



#include "stm8l151c8.h"




#define PS_ON      GPIO_SetBits(GPIOD , GPIO_Pin_0);
#define PS_OFF     GPIO_ResetBits(GPIOD , GPIO_Pin_0);


#define V12_ON      GPIO_SetBits(GPIOC, GPIO_Pin_4);
#define V12_OFF     GPIO_ResetBits(GPIOC, GPIO_Pin_4);


#define LED_ON      GPIO_SetBits(GPIOB, GPIO_Pin_0);
#define LED_OFF     GPIO_ResetBits(GPIOB, GPIO_Pin_0);

#define NET_POWER_ON   GPIO_SetBits(GPIOB, GPIO_Pin_2);
#define NET_POWER_OFF  GPIO_ResetBits(GPIOB, GPIO_Pin_2);














#endif




