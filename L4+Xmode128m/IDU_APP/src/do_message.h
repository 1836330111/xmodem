
#ifndef __DO_MESSAGE_H__
#define __DO_MESSAGE_H__



#include "stm8l151c8.h"





#define RGBY_SPOKE_TIME_US      0x40
#define RGBY_WRITE_CMD			0x30
#define RGBY_TRENDS_CMD			0x3F
#define ECO_SETCURRET_CMD       0x23
#define ADC_READE_CMD0			0x31
#define ADC_READE_CMD1			0x32
#define ADC_READE_CMD2			0x33
#define TEST_CMD				0xF5
#define VERSION_READE_CMD	    0x20
#define PICMODE_SET_CMD			0x34
#define TEMP_MODE_CMD			0x35
#define DEBUG_MSG_CMD			0x36
#define UPDATE_APP_CMD			0x3A
#define STM32_STATUS_CMD		0x3D
#define Temp_Read_Cmd			0x28

#define SHUTTER_CMD             0x2F
#define STEP_CURRENT_MIN        0x01    //01
#define STEP_CURRENT_MAX		0x0A    //10
#define STEP_TIME_MIN			0x0A    //10ms
#define STEP_TIME_MAX  		    0x03E8  // 1s

#define BOARD_DIFF_OFFSET       20

#define MAX_PWM_SIZE 100



extern int index_pwm;
extern uint16_t pwm_buf_0[];
extern uint16_t pwm_buf_1[];

extern uint16_t PwmBufH;
extern uint16_t PwmBufL;


extern float pwm_ratio;






///////////////////////IDU 系统变量/////////////////////////////////


extern uint8_t Laser_psw_st ;

extern uint8_t Laser_cmd_req ;

extern uint8_t Uart1_Send_Count ;

extern uint8_t UartTx_buff[20] ;


extern uint16_t g_uart_delay_count;

extern uint8_t idu_sync_flag ;

extern uint8_t SystemStatus_Flg ;

extern uint8_t Dynamic_contrast_Flag;


///////////////////////////////////////////////////////////////////////

















typedef enum{
    CW_R = 0,
    CW_G = 1,
    CW_B = 2,
    CW_Y = 3,
    CW_END = 0XFF,

};


//色轮最大段数
#define COLOR_INDEX_MAX 8

///////////////色轮配置 config/////////////////////////

//色轮起点颜色
extern __IO uint8_t g_cw_start_color;

//色轮段数
extern __IO uint8_t g_cw_segments_cnt;    

//色轮起点的索引
extern __IO uint8_t g_start_index;

//色轮模型
extern __IO uint8_t g_cw_mode[COLOR_INDEX_MAX];


//各段SPOKE时间 us,微秒  0---65535
extern __IO uint16_t g_cw_spoke_time[COLOR_INDEX_MAX];


//SPOKE开关   0 关闭   1 开启
extern __IO uint8_t Spoke_OnOff;



//各段电流电阻值
//extern __IO uint8_t g_curren_value[COLOR_INDEX_MAX]; 

/////////////////运行状态///////////////////
//当前索引
extern __IO uint8_t g_index;
extern __IO uint16_t g_active_curren_value;
extern __IO uint16_t g_curren_value[];
extern __IO uint16_t g_raw_curren_value[];

extern __IO uint16_t g_resistance_value[];


extern __IO uint8_t current_update_flag;
extern __IO uint8_t g_current_color;


ext volatile uint8_t g_RGB_Present;//当前色段


////////////////////////////////////////////////////////////


typedef struct _resistance_current
{
    uint8_t resistance;
    uint16_t current;
}resistance_current;

extern const resistance_current mcp4131_current_table[];

////////////////////////////////////////////////////////////






//extern uint8_t g_Spoke_St;

#define ENTER_SPOKE {GPIOD->ODR |= 0xA0;\
                    TIM2->CNTRH = 0;\
                    TIM2->CNTRL = 0;\
                    SPOKE_ON;\
                    //g_Spoke_St=1;\
                    }

#define END_SPOKE




void OnIDU_PowerUp_SlowRise(int slowup);



void Temprature_Fan_Ctrl_Handle(void);



void Test_PC(void);



void Get_Current_VALUE(uint8_t* currp_in);
void Get_MCP4131_VALUE(void);

void Get_Ratio_MCP4131_VALUE(void);


#endif




