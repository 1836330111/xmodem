


#include "public.h"



#define VERSION_MAIN   10
#define VERSION_SLAVE  01
#define VERSION_DEV   0


#define Default_Spoke_time_us  350


///////////////////////IDU 系统变量/////////////////////////////////

uint8_t idu_sync_flag = 0;

uint8_t Laser_psw_st = 0;

uint8_t Laser_cmd_req = LASER_ENABLE_REQ;

uint8_t Uart1_Send_Count=0;

uint8_t UartTx_buff[20]={0x5A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x5A};


uint16_t g_uart_delay_count=0;

uint8_t SystemStatus_Flg = 0;



///////////////////////////////////////////////////////////////////////







///////////////色轮配置 config/////////////////////////

//色轮起点颜色
__IO uint8_t g_cw_start_color = CW_R;

//色轮段数
__IO uint8_t g_cw_segments_cnt = 4;    

//色轮起点的索引
__IO uint8_t g_start_index = 0;

//色轮模型
__IO uint8_t g_cw_mode[COLOR_INDEX_MAX]={CW_R,CW_G,CW_B,CW_Y};


//各段SPOKE时间 us,微秒  0---65535
//__IO uint16_t g_cw_spoke_time[COLOR_INDEX_MAX]={300,300,300,300,300};
__IO uint16_t g_cw_spoke_time[COLOR_INDEX_MAX]={Default_Spoke_time_us,Default_Spoke_time_us,Default_Spoke_time_us,Default_Spoke_time_us,200};

//SPOKE开关   0 关闭   1 开启
__IO uint8_t Spoke_OnOff = 1;


//各段电流电阻值
#define CURREN_VALUE_DEFAULT  6
//__IO  uint8_t g_curren_value[COLOR_INDEX_MAX]={15,30,25,15,10}; 
//__IO  uint8_t g_curren_value[COLOR_INDEX_MAX]={50,50,50,50,50}; 
//__IO  uint8_t g_curren_value[COLOR_INDEX_MAX]={CURREN_VALUE_DEFAULT,CURREN_VALUE_DEFAULT,CURREN_VALUE_DEFAULT,CURREN_VALUE_DEFAULT,CURREN_VALUE_DEFAULT,CURREN_VALUE_DEFAULT,}; 


/////////////////运行状态///////////////////



//当前索引
__IO uint8_t g_index = 0;
__IO uint16_t g_active_curren_value = 0;

__IO uint16_t g_curren_value[4] ={0};
__IO uint16_t g_raw_curren_value[4] = {0,0,0,0};

__IO uint16_t g_resistance_value[4] ={0};



__IO uint8_t current_update_flag = 0;
__IO uint8_t g_current_color = 0;


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//uint8_t g_curren_value[COLOR_INDEX_MAX]={0x00,0x00,0x00,0x00,0x00,0x00};
//uint8_t g_curren_slowrise_value[COLOR_INDEX_MAX]={0x00,0x00,0x00,0x00,0x00,0x00};
//uint8_t g_curren_value[COLOR_INDEX_MAX]={5,5,5,5,5,5}; //3.2A----3A
//uint8_t g_curren_value[COLOR_INDEX_MAX]={13,13,13,13,13,13}; //2.7a----2.5A 
//uint8_t g_curren_value[COLOR_INDEX_MAX]={0x90,0x90,0x90,0x90,0x90,0x90};
//uint8_t g_curren_value[COLOR_INDEX_MAX]={100,100,100,100,100,100};

//uint8_t curren_slowrise_value[COLOR_INDEX_MAX]={0x00,0x00,0x00,0x00,0x00,0x00,0x00};

//#define DEBUG_PRINT_MSG  

int index_pwm=0;
uint16_t pwm_buf_0[MAX_PWM_SIZE+5]={0};
uint16_t pwm_buf_1[MAX_PWM_SIZE+5]={0};

uint16_t PwmBufH = 0;
uint16_t PwmBufL = 0;


float pwm_ratio = 1.0;



//uint8_t Laser_psw_st = 0;
//uint8_t Laser_cmd_req = 0;
//uint8_t g_Spoke_St=0;

uint8_t month, day, year,hour,minute,second;
//uint16_t ADC_Data[16]={12,12,12,12,12,12,12,12,12,12};
uint16_t ADC_Data[16]={0};


double  RGB_percent = 1.0;

int8_t RGBY_data[6] = {10,10,10,10,86};//RGBY电流值对应的数字电位器值
uint8_t MCP4131_VALUE[8] = {0};

uint8_t temp_buf[32]={0};

uint16_t temp_spoke_time[8] = {0,0,0,0,0};

uint8_t Dynamic_contrast_Flag = 0;

#define BOARD_OFFSET 10
#if 0
const resistance_current mcp4131_current_table[] = {
        {0, 39700},
        {63, 12200},
        {64, 12100},
        {127, 8490}, 
};


#endif

#if 1
const resistance_current mcp4131_current_table[] = {
        {0, 39700},
        {1, 38000},
        {2, 36500},
        {3, 35000},
        {4, 33700},
        {5, 31500},
        {6, 30500},		
        {7, 29500},     
        {8, 28600},
        {9, 27700},
        {10, 27000},
        {11, 26200},
        {12, 25600},
        {13, 24900},
        {14, 24300},
        {15, 23700},
        {16, 23100},
        {17, 22600},     
        {18, 22100},
        {19, 21600},
        {20, 21200},
        {21, 20900},
        {22, 20400},
        {23, 20000},
        {24, 19900},
        {25, 19500},
        {26, 19200},
        {27, 18900},     
        {28, 18600},
        {29, 18300},
        {30, 18000},
        {31, 17700},
        {32, 17500},
        {33, 17200},
        {34, 16900},
        {35, 16700},
        {36, 16500},
        {37, 16300},     
        {38, 16100},
        {39, 15800},  
        {40, 15600},
        {41, 15400},
        {42, 15200},
        {43, 15000},
        {44, 14800},
        {45, 14700},
        {46, 14500},
        {47, 14400},     
        {48, 14200},
        {49, 14000},
        {50, 13800},
        {51, 13700},
        {52, 13500},
        {53, 13400},
        {54, 13300},
        {55, 13200},
        {56, 13000},
        {57, 12900},     
        {58, 12700},
        {59, 12600},
        {60, 12500},
        {61, 12400},
        {62, 12300},
        {63, 12200},
        {64, 12100},
        {65, 12000},
        {66, 11900},
        {67, 11800},     
        {68, 11700},
        {69, 11600},
        {70, 11500},
        {71, 11400},
        {72, 11300},
        {73, 11200},
        {74, 11100},
        {75, 11000},
        {76, 10900},
        {77, 10900},     
        {78, 10800},
        {79, 10700},
        {80, 10500},
        {81, 10500},
        {82, 10500},
        {83, 10400},
        {84, 10400},
        {85, 10300},
        {86, 10200},
        {87, 10200},
        {88, 10100},     
        {89, 9990},
        {90, 9930},
        {91, 9840},
        {92, 9860},
        {93, 9750},
        {94, 9680},
        {95, 9610},
        {96, 9550},
        {97, 9520},
        {98, 9430},     
        {99, 9360},
        {100, 9360},
        {101, 9280},
        {102, 9200},
        {103, 9150},
        {104, 9110},
        {105, 9060},
        {106, 9010},
        {107, 8910},
        {108, 8890},     
        {109, 8880},
        {110, 8850},  
        {111, 8800},
        {112, 8750},
        {113, 8700},
        {114, 8660},
        {115, 8620},
        {116, 8540},
        {117, 8520},
        {118, 8510},     
        {119, 8510},
        {120, 8510},
        {121, 8510},
        {122, 8510},
        {123, 8515},
        {124, 8510},
        {125, 8405},
        {126, 8200},
        {127, 0}, 
};
#endif

#if 0
const resistance_current mcp4131_current_table[] = {
        {0, 39700},
        {1, 38000},
        {2, 36500},
        {3, 35000},
        {4, 33700},
        {5, 31500},
        {6, 30500},		
        {7, 29500},     
        {8, 28600},
        {9, 27700},
        {10, 27000},
        {11, 26200},
        {12, 25600},
        {13, 24900},
        {15, 23700},
        {16, 23100},   
        {18, 22100},
        {20, 21200},
        {24, 19900},    
        {28, 18600},
        {32, 17500},
        {36, 16500},  
        {40, 15600},
        {44, 14800},   
        {48, 14200},
        {56, 13000},
        {68, 11700},
        {72, 11300},
        {87, 10200},
        {98, 9430},         
        {115, 8620},
        {127, 8000}, 
};


#endif

#if 1
const resistance_current mcp4131_current_table1[] = {
        {5, 31500},
        {6, 30500},		     
        {8, 28600},
        {12, 25600},
        {18, 22100},
        {24, 19900},    
        {32, 17500},
        {48, 14200},
        {68, 11700},    
        {127, 8000}, 
};


#endif



void Get_Dynamic_contrast_Value(uint8_t* Currp_in,uint8_t* Currp_out)
{
    int i =0;
    
	int8_t index = 0;
    uint16_t temp_value = 0;

  	for(index=0;index<4;index++)
	{
        Currp_out[index] = (uint8_t)(Currp_in[index] * pwm_value);
  	}

}


void Get_Current_VALUE(uint8_t* currp_in)
{
    int i =0;
    
	int8_t index = 0;
    uint16_t temp_value = 0;

  	for(index=0;index<4;index++)
	{
      //mcp4131_value[index] = currp_in[index] * 236 + 8000 ;  0.8--3.8
       temp_value  = currp_in[index] * 236 + 8000;       //0.8--3.8
      //temp_value  = currp_in[index] * 299;  0.0--3.5
       g_raw_curren_value[index] = temp_value;
    }

}

void Get_MCP4131_VALUE(void)
{
	int i;
	int index;
	uint16_t temp_value = 0;
	static float pre_pwm_ratio = 0;
	static uint16_t pre_curren_value[4] ={0};
	if(Dynamic_contrast_Flag == 0)
	{	
		for( i=0 ; i<4 ; i++)
		{
			g_curren_value[i]= g_raw_curren_value[i];
			if(g_curren_value[i] >= 31500)
			{
				g_curren_value[i] = 31500;
			}
		}
		for(index=0;index<4;index++)
			{
				if(pre_curren_value[index]!=g_curren_value[index])
				{
					pre_curren_value[index]=g_curren_value[index];
				}
				else
				{
					continue;
				}
				temp_value = g_curren_value[index];
			
				for(i=0 ; i< 128 ; i++)
				{ 
					if(temp_value >= mcp4131_current_table[i].current)
					{
					   g_resistance_value[index] = mcp4131_current_table[i].resistance;
					   break;
					}	
				}
				
			 } 

		
	}
	
    
}



void Get_Ratio_MCP4131_VALUE(void)
{
	uint16_t Temp_PwmBufL = 0;

	

	if(Dynamic_contrast_Flag == 0)
	{   
		;
	}
    else 
    {

         Temp_PwmBufL = (PwmBufL/5 * 25);
         if(g_active_curren_value > Temp_PwmBufL)
         {
             g_active_curren_value--;
		 }
         else if(g_active_curren_value < Temp_PwmBufL)
         {
             g_active_curren_value++;
		 }
		 else;

    }

}


#if 0
void Get_Ratio_MCP4131_VALUE(void)
{
	int i;
	int index;
	uint16_t temp_value = 0;
	static float pre_pwm_ratio = 0;
	static uint16_t pre_curren_value[4] ={0};

	if(Dynamic_contrast_Flag == 0)
	{   
	    for( i=0 ; i<4 ; i++)
        {
		    g_curren_value[i]= g_raw_curren_value[i];
			if(g_curren_value[i] >= 31500)
			{
				g_curren_value[i] = 31500;
			}
	    }
	}
    else 
    {
	     if((pre_pwm_ratio >= (pwm_ratio+0.05))||(pre_pwm_ratio <= (pwm_ratio-0.05)))
	     {
            pre_pwm_ratio = pwm_ratio;
            for( i=0 ; i<4 ; i++)
            {
               g_curren_value[i]= (uint16_t)(g_raw_curren_value[i]* pwm_ratio);
			   if(g_curren_value[i] >= 31500)
			   {
				   g_curren_value[i] = 31500;
			   }
              // g_curren_value[i]= g_raw_curren_value[i];
            }

	     }
    }

#if 1

			for(index=0;index<4;index++)
	        {
                
              #if 0
			    if(pre_curren_value[index]!=g_curren_value[index])
				{
					pre_curren_value[index]=g_curren_value[index];
				}
			    else
				{
					continue;
				}
				#endif
		        temp_value = g_curren_value[index];
			
			    for(i=0 ; i< 10 ; i++)
			    { 
				    if(temp_value >= mcp4131_current_table[i].current)
				    {
					   g_resistance_value[index] = mcp4131_current_table[i].resistance;
					   break;
				    }   
		        }
				
             } 
			#endif
}
#endif



void Func_DiffBoardCurrOffset(uint8_t* currp,uint8_t* currp_out)
{


	int8_t index = 0;

	int8_t off_set = BOARD_OFFSET;

	for(index=0;index<4;index++)
	{
		currp_out[index] = currp[4] - (off_set - currp[index]);

		if(currp_out[index] < 0)
			{
					currp_out[index] = 0;
			}
		
		
		currp_out[index] =  (int8_t)(currp_out[index]);

#if 0
        if((127 - currp_out[index]) >= 0)
            {      
                currp_out[index] = 127 - currp_out[index];
            }
        else
            {
                currp_out[index] = 0;
            }
#endif      


        
    }	


#if 0
//127 - (127-20+x*0.8)
	int8_t index = 0;

	int8_t off_set = BOARD_OFFSET;

	for(index=0;index<4;index++)
	{
		currp_out[index] = currp[4] - (off_set - currp[index]);

		if(currp_out[index] < 0)
			{
					currp_out[index] = 0;
			}
		
		
		currp_out[index] =  (int8_t)(currp_out[index] * RGB_percent);

        if((127 - currp_out[index]) >= 0)
            {      
                currp_out[index] = 127 - currp_out[index];
            }
        else
            {
                currp_out[index] = 0;

            }

        
    }	
	#endif

#if 0
//127 - (127-20+x*0.8)
	int8_t index = 0;

	int8_t off_set = BOARD_OFFSET;

	for(index=0;index<4;index++)
	{
		*(currp+index) = *(currp+4) - (off_set - (*(currp+index)));

		if(*(currp+index) < 0)
			{
					*(currp+index) = 0;
			}
		*(currp+index) = 127 - *(currp+index);
    }	
#endif


#if 0
	uint8_t rgby_data_max = 0;
	rgby_data_max = 127 - BOARD_DIFF_OFFSET;

	uint8_t index = 0;
 	float Val_coe= 0.8;
	
	//*(currp+4) = MIN((*(currp+4)),rgby_data_max);
	for(index=0;index<4;index++)
	{
		*(currp+index) = 127 - ((int8_t)((float)(*(currp+index))*Val_coe));
    }
	
	//*(currp+4) += BOARD_DIFF_OFFSET;
#endif



	
}

uint8_t Com_checkSum(uint8_t* pbuf)
{
	uint8_t i,sum = 0;
	for(i=0;i<19;i++)
		sum += pbuf[i];
	return sum&0xff;
}

#if  0
void ReadCurrent_Handler(void)
{
	uint8_t u8_valindx= 0,u8_arrindx = 0;
	uint16_t data_offset = 15800;
	uint16_t value[8] = {0};
	float Val_coe= 5.374;

	for(u8_valindx = 0;u8_valindx < 8;u8_valindx += 2)
	{
		value[u8_valindx] = (uint16_t)(ADC_Data[u8_arrindx]*Val_coe - data_offset);
		value[u8_valindx+1] = (uint16_t)(ADC_Data[u8_arrindx+1]*Val_coe - data_offset);
		u8_arrindx += 4;
	}
	
	memset(UartTx_buff,0,20);
	UartTx_buff[0]=0xCF;
	UartTx_buff[1]= (uint8_t)(value[0] >> 8 &0xFF);
	UartTx_buff[2]= (uint8_t)(value[0] & 0xFF);
	UartTx_buff[3]=0x00; 
	UartTx_buff[4]=UartRx_buff[4]; 
	UartTx_buff[5] = (uint8_t)(value[1] >> 8 & 0xFF);
	UartTx_buff[6] = (uint8_t)(value[1] & 0xFF);
	UartTx_buff[7] = (uint8_t)(value[2] >> 8 & 0xFF);
	UartTx_buff[8] = (uint8_t)(value[2] & 0xFF);
	UartTx_buff[9] = (uint8_t)(value[3] >> 8 & 0xFF);
	UartTx_buff[10] = (uint8_t)(value[3] & 0xFF);
	UartTx_buff[11] = (uint8_t)(value[4] >> 8 & 0xFF);
	UartTx_buff[12] = (uint8_t)(value[4] & 0xFF);
	UartTx_buff[13] = (uint8_t)(value[5] >> 8 & 0xFF);
	UartTx_buff[14] = (uint8_t)(value[5] & 0xFF);
	UartTx_buff[15] = (uint8_t)(value[6] >> 8 & 0xFF);
	UartTx_buff[16] = (uint8_t)(value[6] & 0xFF);
	UartTx_buff[17] = (uint8_t)(value[7] >> 8 & 0xFF);
	UartTx_buff[18] = (uint8_t)(value[7] & 0xFF);
	UartTx_buff[19]=Com_checkSum(UartTx_buff);

	DMA_Cmd(DMA1_Channel1, DISABLE);
	DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
	DMA_Cmd(DMA1_Channel1, ENABLE);
	
}
#endif

void Date_Time_Int(void)
{
	char date_t [] = __DATE__;
	char time_t [] = __TIME__;
	int i = 0;
	char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

	year = atoi(date_t + 9);
	*(date_t + 6) = 0;
	day = atoi(date_t + 4);
	*(date_t + 3) = 0;
	for (i = 0; i < 12; i++){
	if (!strcmp(date_t, months[i])){
		month = i + 1;
		break;
		}
	}
	second = atoi(time_t + 6);
	*(time_t + 5) = 0;
	minute = atoi(time_t + 3);
	*(time_t + 2) = 0;
	hour = atoi(time_t);
}


void Versions_Task(pDECODE_TABLE  pd)
{
	memset(UartTx_buff,0,20);
	UartTx_buff[0]=0xC0 |(pd->cmd_buf[0] >>4);
	UartTx_buff[3]=0; 
	UartTx_buff[4]=0x20;
	UartTx_buff[10]=VERSION_MAIN;
	UartTx_buff[11]=VERSION_SLAVE;
	UartTx_buff[12]=year-15;
	UartTx_buff[13]=month;
	UartTx_buff[14]=day;
	UartTx_buff[15]=hour;
	UartTx_buff[16]=minute;
	UartTx_buff[17]=second;
	UartTx_buff[19]=Com_checkSum(UartTx_buff);

	DMA_Cmd(DMA1_Channel1, DISABLE);
	DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
	DMA_Cmd(DMA1_Channel1, ENABLE);
}



void Test_PC(void)
{
    UartTx_buff[0]=0xCA;                
    UartTx_buff[3]=0X00; 
    UartTx_buff[4]=0x99; 
    UartTx_buff[14]= ADC_Data[2]; 
    UartTx_buff[15]= ADC_Data[10]; 
    UartTx_buff[19]=Com_checkSum(UartTx_buff);
    DMA_Cmd(DMA1_Channel1, DISABLE);
    DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
    DMA_Cmd(DMA1_Channel1, ENABLE);

}




int Do_Msg(pDECODE_TABLE  pd)
{
	int8_t  Temp_RGB[6]={0};
	int8_t  Temp_RGB1[6]={0};
	if(pd->cmd_buf[4]==SHUTTER_CMD)
	{
		if(pd->cmd_buf[14] ==0x01)
		{
			Laser_cmd_req &= ~LASER_ENABLE_REQ;
		}
		else
		{
			Laser_cmd_req |= LASER_ENABLE_REQ;
		}
	}
	else if(pd->cmd_buf[4]==STM32_STATUS_CMD)
    {
        ;//Test_PC();
    } 
	else if(pd->cmd_buf[4]==RGBY_WRITE_CMD)
	{

		Dynamic_contrast_Flag = pd->cmd_buf[5];

		//Spoke_OnOff=pd->cmd_buf[6] & 0x01;
		if(pd->cmd_buf[6] & 0x01)
		{
			g_cw_spoke_time[0]=180;
			g_cw_spoke_time[1]=350;
			g_cw_spoke_time[2]=180;
			g_cw_spoke_time[3]=180;

		}
		else
		{
			g_cw_spoke_time[0]=0;
			g_cw_spoke_time[1]=0;
			g_cw_spoke_time[2]=Default_Spoke_time_us;
			g_cw_spoke_time[3]=0;
		}

 		memcpy(RGBY_data,pd->cmd_buf+14,5);
		Func_DiffBoardCurrOffset(RGBY_data,Temp_RGB);
        Get_Current_VALUE(Temp_RGB);
		Get_MCP4131_VALUE();
		//memcpy((uint8_t*)g_curren_value,(uint8_t*)MCP4131_VALUE,4);
		
		
	}
	else if(pd->cmd_buf[4]==ECO_SETCURRET_CMD)
	{
        RGBY_data[4] = pd->cmd_buf[5];
        Func_DiffBoardCurrOffset(RGBY_data,Temp_RGB);
        Get_Current_VALUE(Temp_RGB);
		Get_MCP4131_VALUE();
	//	memcpy((uint8_t*)g_curren_value,(uint8_t*)MCP4131_VALUE,4);
		

	}
	else if(pd->cmd_buf[4]==RGBY_SPOKE_TIME_US)
	{
        memcpy((uint8_t*)g_cw_spoke_time,&pd->cmd_buf[5],10);
	}
    
	else if(pd->cmd_buf[4]==VERSION_READE_CMD)
	{
       Versions_Task( pd);
	}
	else if(pd->cmd_buf[4]==PICMODE_SET_CMD)
	{
		if(pd->cmd_buf[6] & 0x01)
		{
			g_cw_spoke_time[0]=180;
			g_cw_spoke_time[1]=350;
			g_cw_spoke_time[2]=180;
			g_cw_spoke_time[3]=180;
		}
		else
		{
			g_cw_spoke_time[0]=0;
			g_cw_spoke_time[1]=0;
			g_cw_spoke_time[2]=Default_Spoke_time_us;
			g_cw_spoke_time[3]=0;
		}

		memcpy(RGBY_data,pd->cmd_buf+14,5);
		Func_DiffBoardCurrOffset(RGBY_data,Temp_RGB);
		Get_Current_VALUE(Temp_RGB);
		Get_MCP4131_VALUE();
		//memcpy((uint8_t*)g_curren_value,(uint8_t*)MCP4131_VALUE,4);


	}
	else if(pd->cmd_buf[4]==ADC_READE_CMD1)
	{
        UartTx_buff[0]=0xCB;				
		UartTx_buff[3]=0X00; 
		UartTx_buff[4]=pd->cmd_buf[4]; 
		UartTx_buff[14]= ADC_Data[2]; 
		UartTx_buff[15]= ADC_Data[10]; 
		UartTx_buff[19]=Com_checkSum(UartTx_buff);
		DMA_Cmd(DMA1_Channel1, DISABLE);
		DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
		DMA_Cmd(DMA1_Channel1, ENABLE);
	}
	else if(pd->cmd_buf[4]==TEST_CMD)
	{
		static uint32_t cmd_cnt;
		memset(UartTx_buff,0,20);
		UartTx_buff[0]=0xCA;				
		UartTx_buff[3]=0X00; 
		UartTx_buff[4]=pd->cmd_buf[4]; 
		UartTx_buff[5]=pd->cmd_buf[5]; 
		if((pd->cmd_buf[5] == 0x00)||(pd->cmd_buf[5] == 0x01))
		{
			cmd_cnt++;
		}
		else if(pd->cmd_buf[5] == 0x03)
		{
		
			cmd_cnt=0;
		}
		else
		{
			UartTx_buff[10]=(uint8_t)(cmd_cnt&0xFF); 
			UartTx_buff[11]=(uint8_t)((cmd_cnt>>8)&0xFF); 
			UartTx_buff[12]=(uint8_t)((cmd_cnt>>16)&0xFF);
			UartTx_buff[13]=(uint8_t)((cmd_cnt>>24)&0xFF);
		}
		UartTx_buff[19]=Com_checkSum(UartTx_buff);
		DMA_Cmd(DMA1_Channel1, DISABLE);
		DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
		DMA_Cmd(DMA1_Channel1, ENABLE);
	}		
	else if(pd->cmd_buf[4]==UPDATE_APP_CMD)
	{
		if(pd->cmd_buf[6] == 0x01)
		{					
			FLASH_Unlock(FLASH_MemType_Program);//置烧录标志	
			FLASH_EraseByte(0xC010);
			FLASH_ProgramByte(0xC010,0xA5);
			FLASH_Lock(FLASH_MemType_Program);
			asm("LDW X,  SP ");
			asm("LD  A,  $FF");
			asm("LD  XL, A	");
			asm("LDW SP, X	");
			asm("JPF $8000");	//重启跳入IAP 
		}

	}
	else if(pd->cmd_buf[4]==Temp_Read_Cmd)
		{
			UartTx_buff[0]=0xCB;				
			UartTx_buff[3]=0X00; 
			UartTx_buff[4]=pd->cmd_buf[4]; 
			UartTx_buff[14]= ADC_Data[2]; 
			UartTx_buff[15]= ADC_Data[10]; 
			UartTx_buff[19]=Com_checkSum(UartTx_buff);
			DMA_Cmd(DMA1_Channel1, DISABLE);
			DMA_SetCurrDataCounter(DMA1_Channel1, 20);//函数重新设置待发送的数据大小，之后再使能DMA通道
			DMA_Cmd(DMA1_Channel1, ENABLE);
		}


	return 0;
}


void Msg_Handle(void)
{
	int res=0;

	res = ppfifo_out(Decode_Lx.pf,temp_buf);
	for(int i=0;i<res;i++)
	{         
		CMD_Lx_Decode(&Decode_Lx,temp_buf[i],0);
		if(Decode_Lx.cmd_flag == 1)
		{
	        Decode_Lx.cmd_flag =0;
			Do_Msg(&Decode_Lx);
		}		
	}
}
	double pwm_total_0 = 0;
	double pwm_total_1 = 0;
        double pwm_average_0 = 0;
	double pwm_average_1 = 0;

	#if 0
void Get_Dynamic_contrast_Average_Value(void)
{
    uint16_t Temp_Buf_Indix = 0;
	uint16_t i = 0;
	uint16_t pwm_max_0 = 0;
	uint16_t pwm_max_1 = 0;
	uint16_t pwm_min_0 = pwm_buf_0[0];
	uint16_t pwm_min_1 = pwm_buf_1[0];

          
        pwm_total_0 = 0;
	 pwm_total_1 = 0;
         
	for(i=0; i < MAX_PWM_SIZE; i++)
	{
               if(pwm_max_0 < pwm_buf_0[i])
                {
                        pwm_max_0 = pwm_buf_0[i];
                }
               if(pwm_max_1 < pwm_buf_1[i])
                {
                        pwm_max_1 = pwm_buf_1[i];
                }
               if(pwm_min_0 > pwm_buf_0[i])
                {
                        pwm_min_0 = pwm_buf_0[i];
                }
               if(pwm_min_1 > pwm_buf_1[i])
                {
                        pwm_min_1 = pwm_buf_1[i];
                }
       
       
		pwm_total_0 =pwm_total_0 + pwm_buf_0[i];
		pwm_total_1 =pwm_total_1 + pwm_buf_1[i];
	}



	pwm_average_0 = (pwm_total_0 - pwm_max_0 - pwm_min_0)/(MAX_PWM_SIZE-2);
	pwm_average_1 = (pwm_total_1 - pwm_max_1 - pwm_min_1)/(MAX_PWM_SIZE-2);
		
    pwm_ratio = (pwm_buf_1[1]/(pwm_buf_1[1]+pwm_buf_0[1]));
	if(pwm_ratio >= 0.85)
	{
       pwm_ratio = 1;
	}
   // pwm_ratio = pwm_ratio;
}
#endif


extern uint16_t pwm_total;
extern uint16_t pwm_count;

extern uint8_t aa;
extern uint8_t bb;
extern uint32_t aaa;
extern uint32_t bbb;


#if 1
void Get_Dynamic_contrast_Average_Value(void)
{	
    if(pwm_count >= 100)
    {
    		   if(aa == 0)
    	       {
					aaa = System_Tick;
					aa = 1;
			   }
		PwmBufL = pwm_total/pwm_count;
		pwm_count = 0;
		pwm_total = 0;
					   if(bb == 0)
		       {
				    bbb = System_Tick;
				    bb = 1;

	           }
	}
}
#endif

#if 0
void DMA_Config(void)
{
	DMA_GlobalDeInit();
	DMA_DeInit(DMA1_Channel0);
	SYSCFG_REMAPDMAChannelConfig(REMAP_DMA1Channel_ADC1ToChannel0);
	DMA_Init(DMA1_Channel0, BUFFER_ADDRESS,
	       ADC1_DR_ADDRESS,
	       BUFFER_SIZE,
	       DMA_DIR_PeripheralToMemory,
	       DMA_Mode_Circular,
	       DMA_MemoryIncMode_Inc,
	       DMA_Priority_High,
	       DMA_MemoryDataSize_HalfWord);
	DMA_Cmd(DMA1_Channel0, ENABLE);
	//DMA_ITConfig(DMA1_Channel0, DMA_ITx_TC, ENABLE);
	DMA_GlobalCmd(ENABLE);
}
#endif

#if 0
void Adc_DMA_Config(void)
{
  DMA_GlobalDeInit();
  DMA_DeInit(DMA1_Channel0);
  SYSCFG_REMAPDMAChannelConfig(REMAP_DMA1Channel_ADC1ToChannel0);
  DMA_Init(DMA1_Channel0, BUFFER_ADDRESS,
           ADC1_DR_ADDRESS,
           BUFFER_SIZE,
           DMA_DIR_PeripheralToMemory,
           DMA_Mode_Circular,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_High,
           DMA_MemoryDataSize_HalfWord);

  DMA_ITConfig(DMA1_Channel0, DMA_ITx_TC, DISABLE);
  DMA_ITConfig(DMA1_Channel0, DMA_ITx_HT, DISABLE);
  DMA_Cmd(DMA1_Channel0, ENABLE);
  
  DMA_GlobalCmd(ENABLE);
}
#endif

void DMA_Config_Uart(void)
{
  DMA_GlobalDeInit();
  DMA_DeInit(DMA1_Channel1);
  //SYSCFG_REMAPDMAChannelConfig(REMAP_DMA1Channel_ADC1ToChannel0);
  DMA_Init(DMA1_Channel1, (uint32_t)UartTx_buff,
           (uint16_t)0x5231,
           20,
           DMA_DIR_MemoryToPeripheral,
           DMA_Mode_Normal,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_High,
           DMA_MemoryDataSize_Byte);
  #if 0
  DMA_DeInit(DMA1_Channel2);
  //SYSCFG_REMAPDMAChannelConfig(REMAP_DMA1Channel_ADC1ToChannel0);
  DMA_Init(DMA1_Channel2,(uint32_t)pd->cmd_buf,
           (uint16_t)0x5231,
           20,
           DMA_DIR_PeripheralToMemory,
           DMA_Mode_Circular,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_High,
           DMA_MemoryDataSize_Byte);
  DMA_Cmd(DMA1_Channel2, ENABLE);
  DMA_ITConfig(DMA1_Channel2,DMA_ITx_TC,ENABLE); 
  #endif

  DMA_GlobalCmd(ENABLE);
}


void OnIDU_PowerUp_SlowRise(int slowup)
{
    uint8_t i;
    uint8_t dst_value=0;
    if(slowup > RGBY_DATA_MIN)
        {
            dst_value = slowup;
        }
    else
        {
            dst_value = RGBY_DATA_MIN;
        }


    for(i = 100 ; i>dst_value; i=i-1)
    {
        g_active_curren_value = i;
        Current_Handle_Value();
        delayms(10);
    }

}




void Set_Curren_Value(uint8_t CW,uint8_t value)
{

    int i=0;
    for(i=0; i<g_cw_segments_cnt; i++)
        {
            if(g_cw_mode[i]==CW)
                {
                    g_curren_value[i] =173-value;
                }
        }


}

void Set_Curren_And_Spoke_Delay(uint8_t CW,uint8_t* pvalue)
{

    int i=0;
    for(i=0; i<g_cw_segments_cnt; i++)
        {
            if(g_cw_mode[i]==CW)
                {
                    g_curren_value[i] =255-pvalue[0];
                    if(pvalue[5]==1)
                    {
                        g_cw_spoke_time[i] = pvalue[6]<<8 | pvalue[7];
                    }
                }
        }


}


void Uart_RE_Handle(void)
{

	if(((Laser_cmd_req & UART1_RECV_REQ) != 0)&&(Uart1_Send_Count==0))
	{
		Laser_cmd_req  &= ~UART1_RECV_REQ;
		UartTx_buff[0]= 0x5A; 
		UartTx_buff[4]=Do_Recv_Cmd[4]; 

		if((Do_Recv_Cmd[0] == 0xA5) &&(Do_Recv_Cmd[15] == 0xA5))
		{
			switch(Do_Recv_Cmd[4])
			{

				case IDU_CURRENT_R:
              
                  Set_Curren_And_Spoke_Delay(CW_R,&Do_Recv_Cmd[6]);
				  if(Do_Recv_Cmd[6] == 0)
				  {
                       LARSE_OFF;
				  }
                    //Set_Curren_Value(CW_R,Do_Recv_Cmd[6]);
				break;
                
				case IDU_CURRENT_G:
                    Set_Curren_And_Spoke_Delay(CW_G,&Do_Recv_Cmd[6]);
                    //Set_Curren_Value(CW_G,Do_Recv_Cmd[6]);
				break;
                
				case IDU_CURRENT_B:
                    Set_Curren_And_Spoke_Delay(CW_B,&Do_Recv_Cmd[6]);
                    //Set_Curren_Value(CW_B,Do_Recv_Cmd[6]);
                    idu_sync_flag= 1;
				break;
                
				case IDU_CURRENT_Y:
                    Set_Curren_And_Spoke_Delay(CW_Y,&Do_Recv_Cmd[6]);
                    //Set_Curren_Value(CW_Y,Do_Recv_Cmd[6]);
				break;
                
				case CW_INDEX_DELAY_CMD:
				{
				//
				}
				break;

				case IDU_SPOKE_CTR:
				{
                    //spoke on off
                   // Spoke_OnOff  = Do_Recv_Cmd[6];
                  
				 
				}
				break;
                
                case IDU_SPOKE_DELAY:
                {


                   
                }
                break;

                

				
				case IDU_ONLINE_CHK_CMD:
				{
				//
				}   
				break;


             
				case IDU_FANSWITCH:
				{
                  if(Do_Recv_Cmd[6])
                  {
                      User_Set_PWM_Value(400);
                      
                  }
                  else
                  {
                      User_Set_PWM_Value(0);
                  }
                  
				}
				break;

                #if 0
				case IDU_FANSPEED_SET_CMD:
				{
					uint32_t speed;
					UartRx_buff[6] = (UartRx_buff[6] < 0x0F)?(0x0F):UartRx_buff[6];
					speed = (uint32_t)UartRx_buff[6] * 3;
					FanSpeed_SetFunc(speed);
				}
				break;
				case IDU_FANSPEED_ERR_CMD:
				{

				} 
				break;
				case IDU_TEMP_ERR_CMD:
				{

				} 
				break;

				case UPDATE_APP_CMD:
				{
					//uint32_t pBufPartIndex,pBufPartSize,pBufPartCRC;
					//memset(UartTx_buff,0,20);
					if(UartRx_buff[6] == 0x01)
					{					
						FLASH_Unlock(FLASH_MemType_Program);//置烧录标志	
						FLASH_EraseByte(0xC010);
						FLASH_ProgramByte(0xC010,0xA5);
						FLASH_Lock(FLASH_MemType_Program);
						asm("LDW X,  SP ");
						asm("LD  A,  $FF");
						asm("LD  XL, A  ");
						asm("LDW SP, X  ");
						asm("JPF $8000");	//重启跳入IAP 
					}
				}
				break;	
				#endif
				default:
					break;
			}
			//p_RGB_data = RGBY_data; 			
		}
	else if((Do_Recv_Cmd[0] == 0x5A) &&(Do_Recv_Cmd[15] == 0xA5))
	{
			//memset(UartTx_buff,0x00,16);
			UartTx_buff[0] = 0x5A;
			Laser_cmd_req |=UART1_SEND_REQ;
			Uart1_Send_Count = UART_BUFF_SIZE;			
			switch(Do_Recv_Cmd[4])
			{
				case IDU_CURRENT_R:
					;//UartTx_buff[6] = RGBY_data[0];
				break;
				case IDU_CURRENT_G:
					;//UartTx_buff[6] = RGBY_data[1];
				break;
				case IDU_CURRENT_B:
					;//UartTx_buff[6] = RGBY_data[2];
				break;
				case IDU_CURRENT_Y:
					;//UartTx_buff[6] = RGBY_data[3];
				break;
				case IDU_VER_CMD: 
				{		
					;//Versions_Task();
				}
				break;

				case IDU_TEMP_CHK_CMD:
				{
					;//UartTx_buff[6] = GetTempValue();//(uint8_t)(adc_Buffer[0] & 0xff);//

				} 
				break;


				case IDU_FANSPEED_CHK_CMD:
				{
					;//UartTx_buff[5] = (uint8_t)(SignalFrequency >> 8)&0xff;
					;//UartTx_buff[6] = (uint8_t)SignalFrequency &0xff;
				}   
				break;
				default:
					break;
			}
			;//UartTx_buff[14] = Com_checkSum(UartTx_buff);
			UartTx_buff[15] = 0x5A;
			
			DMA_Cmd(DMA1_Channel1, DISABLE);
			DMA_SetCurrDataCounter(DMA1_Channel1, 16);//函数重新设置待发送的数据大小，之后再使能DMA通道
			DMA_Cmd(DMA1_Channel1, ENABLE);
	}
   }

}



uint16_t g_active_ntc_value = 0;
uint8_t g_active_temprature_value = 0;



uint8_t temprature_value[20]={35  ,40  ,42  ,45  ,50  ,55 ,60 ,65 ,70 ,75 ,80 ,85 ,90 ,92 ,100,110};
uint16_t ntc_adc_value[20]  ={1555,1400,1300,1200,1050,940,830,760,680,630,560,510,470,450,400,330};



uint8_t Get_Temprature_Value(uint16_t ntc_value)
{

    int i=0;
    int cnt =sizeof(temprature_value)-1;
    for(i = cnt ; i> 0 ; i--)
        {

            if(ntc_value < ntc_adc_value[i])
                {
                    return temprature_value[i];
                }
        }


    return temprature_value[0];
}




void Temprature_Set_Pwm(void)
{

    static uint8_t pre_active_temprature_value = 0;

    if(pre_active_temprature_value==g_active_temprature_value)
        {
            return ;
        }


    if (pre_active_temprature_value >= 100)
    {
        LARSE_OFF;
        LED_RED_ON;
        User_Set_PWM_Value(630);
    }
    else if(pre_active_temprature_value >= 90)
    {
        LED_RED_ON;
        User_Set_PWM_Value(630);

    }
    else if(pre_active_temprature_value>= 80)
    {

        User_Set_PWM_Value(550);

    }
    else if(pre_active_temprature_value>= 70)
    {

        if(Laser_psw_st == 1)
        {
            LARSE_ON;
            LED_RED_OFF;

        }
    
        User_Set_PWM_Value(450);

    }
    else if(pre_active_temprature_value >= 60)
    {
        //User_Set_PWM_Value(350);  // 50% pwm
        User_Set_PWM_Value(400);    // 61 %  pwm

    }
    else if(pre_active_temprature_value >= 50)
    {
        User_Set_PWM_Value(400); 

    }
    else
    {

       User_Set_PWM_Value(400);


    }

    pre_active_temprature_value = g_active_temprature_value;


}

void Temprature_Fan_Ctrl_Handle(void)
{

    if(Laser_psw_st!=1)return ;
     g_active_ntc_value = Get_Adc_Value();


     g_active_temprature_value = Get_Temprature_Value(g_active_ntc_value);
     
 
    Temprature_Set_Pwm();
  

}







