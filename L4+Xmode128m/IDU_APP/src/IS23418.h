
#ifndef __IS23418_H__
#define __IS23418_H__



#include "stm8l151c8.h"



#ifndef IS234XX_SPI_SPEED  


//16M  ��Ƶ

#define IS234XX_SPI_SPEED SPI_BaudRatePrescaler_16  //SPI_BaudRatePrescaler_8//SPI_BaudRatePrescaler_16


#endif




#define LD_SPI                               SPI1
#define LD_SPI_PIN                       GPIOB 
#define LD_SPI_CLK                       CLK_Peripheral_SPI1
#define LD_SPI_SCK_PIN               GPIO_Pin_5                 /* PB.05 */
#define LD_SPI_MISO_PIN             GPIO_Pin_7                  /* PB.07 */
#define LD_SPI_MOSI_PIN             GPIO_Pin_6                  /* PB.06 */



#define CMD_NOP     			0x00
#define CMD_ACR_READ 			0x20
#define CMD_ACR_WRTE			0x60
#define CMD_REG_READ 			0x80
#define CMD_REG_WRTE 			0xC0



#define LD_SPI_CS_1                     GPIO_Pin_0                   /* Pb0 */
#define LD_SPI_CS_2                     GPIO_Pin_0                   /* Pf0 */
#define LD_SPI_CS_3                     GPIO_Pin_6                  /* Pd6 */
#define LD_SPI_CS_4                     GPIO_Pin_4                   /* Pd4 */
#define REG_ACR				       	0x10
#define REG_WR 					   0x00





void SPI_INIT(void);

uint8_t SPI_SENDREADByte(uint8_t byte);


void ISL23418_write(uint8_t ID,uint8_t reg, uint8_t data);

void ISL23418_read(uint8_t ID,uint8_t *pData, uint8_t reg);


void ISL23418_data(uint8_t reg, uint8_t data);


void ISL23418_up(uint8_t data);


void ISL23418_down(void);

void ISL23418_REG_write(uint8_t ID,uint8_t reg, uint8_t data);

void ISL23425_up(uint8_t data);


void ISL23425_down(void);

void ISL23425_Sine_Loop(uint8_t data);


#endif




