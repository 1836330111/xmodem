





#include "public.h"


#define TIM2_Period     0xFFFF
#define TIM3_Period     0xFFFF



uint16_t Time3_count = 0;
uint16_t Time2_count = 0;



__IO uint32_t System_Tick=0;


 

void  Get_System_Tick(uint8_t* buf)
{

	memcpy(buf,(uint8_t*)&System_Tick,4);

}





/******************************************************************************
                TIM1初始化：
*******************************************************************************/

void User_Tim1_Init(void)
{
//1ms int
    TIM1_TimeBaseInit(15,TIM1_CounterMode_Up,99,0);
	TIM1_ITConfig(TIM1_IT_Update, ENABLE);
	TIM1_Cmd(ENABLE);
}

void User_Tim1_Init_For_DIFF_RG_Y(void)
{
    TIM1_TimeBaseInit(16,TIM1_CounterMode_Up,2000,0);
	//TIM1_ITConfig(TIM1_IT_Update, ENABLE);
	//TIM1_SelectOnePulseMode(TIM1_OPMode_Single);
	TIM1_SetCounter(0);
	TIM1_Cmd(ENABLE);
}

void User_Tim1_Init_For_Delay(void)
{
    TIM1_TimeBaseInit(16000,TIM1_CounterMode_Up,4100,0);
    TIM1_ARRPreloadConfig(ENABLE);
	TIM1_ITConfig(TIM1_IT_Update, DISABLE);
	//TIM1_SelectOnePulseMode(TIM1_OPMode_Single);
	TIM1_SetCounter(0);
	TIM1_Cmd(ENABLE);
}

void User_Tim1_Init_For_Dynamic_contrast(void)
{
    GPIO_Init(GPIOE, GPIO_Pin_7, GPIO_Mode_In_FL_No_IT); 
	TIM1_DeInit();
    SYSCFG_RITIMInputCaptureConfig(RI_InputCapture_IC3,RI_InputCaptureRouting_6);
	TIM1_TimeBaseInit(159,TIM1_CounterMode_Up,5000,0);
	
	TIM1_ICInit(TIM1_Channel_3, TIM1_ICPolarity_Rising, TIM1_ICSelection_DirectTI,
               TIM1_ICPSC_DIV1, 0x00);
	
        TIM1_ClearFlag(TIM1_FLAG_CC3);
	TIM1_ITConfig(TIM1_IT_CC3, ENABLE);


	
        //TIM1_ITConfig(TIM1_IT_Update, ENABLE);
	TIM1_CCxCmd(TIM1_Channel_3, ENABLE);
	//TIM1_SetCounter(0);
	TIM1_Cmd(ENABLE);
}




#if 0
void User_Tim1_Init(void)
{
	/* configure TIM1 channel 2 to capture a PWM signal */
	TIM1_PWMIConfig(TIM1_Channel_2, TIM1_ICPolarity_Rising, TIM1_ICSelection_DirectTI,
	TIM1_ICPSC_DIV1, 0);

	/* Select the TIM1 Input Trigger: TI1FP2 */
	TIM1_SelectInputTrigger(TIM1_TRGSelection_TI2FP2);
	TIM1_SelectSlaveMode(TIM1_SlaveMode_Reset);

	/* Enable CC2 interrupt request */
	TIM1_ITConfig(TIM1_IT_CC2, ENABLE);
	//enableInterrupts();

	/* Enable TIM1 */
	TIM1_Cmd(ENABLE);
}
#endif
/******************************************************************************
                TIM2初始化：设置延时精度 0.25us
*******************************************************************************/
void User_Tim2_Init(void)
{
	TIM2_DeInit();
    //TIM2_TimeBaseInit(TIM2_Prescaler_8 , TIM2_CounterMode_Up , TIM2_Period);
    TIM2_TimeBaseInit(TIM2_Prescaler_4 , TIM2_CounterMode_Up, TIM2_Period);
    TIM2_ClearFlag(TIM2_FLAG_Update);
    //TIM2_ITConfig(TIM2_IT_Update , ENABLE);
    TIM2_SetCounter(0);
    TIM2_Cmd(ENABLE);
	
}

/******************************************************************************
                TIM3初始化：设置延时精度 0.25us
*******************************************************************************/

#if 0
void User_Tim3_Init(void)
{
    TIM3_DeInit();
    TIM3_TimeBaseInit(TIM3_Prescaler_128 , TIM3_CounterMode_Up , TIM3_Period);
    TIM3_ClearFlag(TIM3_FLAG_Update);
    TIM3_ITConfig(TIM3_IT_Update , ENABLE);
    TIM3_SetCounter(0);
    TIM3_Cmd(ENABLE);
}
#endif

void User_Tim3_For_Y_SYNC(void)
{

	TIM3_DeInit();
	TIM3_TimeBaseInit(TIM3_Prescaler_16 , TIM3_CounterMode_Up , TIM3_Period); //1us
	TIM3_ClearFlag(TIM3_FLAG_Update);
	TIM3_SetCounter(0);
	TIM3_Cmd(ENABLE);

}










/******************************************************************************
                TIM4初始化：设置延时精度1ms 中断system tick
*******************************************************************************/

void User_Tim4_Init(void)
{


    TIM4_DeInit();
	
    TIM4_TimeBaseInit(TIM4_Prescaler_16 , 999);// 16M  ,1ms 中断
	
    TIM4_ClearFlag(TIM4_FLAG_Update);
	
    TIM4_ITConfig(TIM4_IT_Update , ENABLE);
	
    TIM4_SetCounter(0);
	
    TIM4_Cmd(ENABLE);

}

void User_Tim4_Init_For_Delay(void)
{
    TIM4_DeInit();
	
    TIM4_TimeBaseInit(TIM4_Prescaler_16384,0);
	
    TIM4_ClearFlag(TIM4_FLAG_Update);
	
    TIM4_ITConfig(TIM4_IT_Update , ENABLE);
	
    TIM4_SetCounter(0);
	
    TIM4_Cmd(ENABLE);

}


void User_Tim4_For_Uart_Timeout(void)
{


    TIM4_DeInit();
	
    TIM4_TimeBaseInit(TIM4_Prescaler_32768 , 240); //    2ms 加一
	
    TIM4_ClearFlag(TIM4_FLAG_Update);
	
    TIM4_ITConfig(TIM4_IT_Update , DISABLE);
	TIM4_ARRPreloadConfig(ENABLE);
    TIM4_SetCounter(0);
	//TIM4_SelectOnePulseMode(TIM4_OPMode_Single);
    TIM4_Cmd(ENABLE);

}


void User_Tim4_For_Uart_Timeout_Handle(void)
{


 
    if(TIM4->CNTR > 10)
        {
            g_uart_delay_count++;
            TIM4->CNTR=0;
           // GPIOD->ODR ^= GPIO_Pin_5;
            //GPIO_ToggleBits(GPIOD,GPIO_Pin_5);
        }
    

}




/******************************************************************************
                TIM5初始化：设置延时精度200us 中断
                for debug  spoke
*******************************************************************************/
    
void User_Tim5_Init_For_Spoke(void)
{

    TIM5_DeInit();
    TIM5_TimeBaseInit(TIM5_Prescaler_16 ,TIM5_CounterMode_Up, 300); 
    
    TIM5_SelectOnePulseMode(TIM5_OPMode_Single);

    
    TIM5_ClearFlag(TIM5_FLAG_Update);
    TIM5_ARRPreloadConfig(ENABLE);
    TIM5_ITConfig(TIM5_IT_Update , ENABLE);
    
    TIM5_SetCounter(200);
    //TIM5->CR1 |= TIM_CR1_CEN;
    //TIM5_Cmd(ENABLE);




}
    

void User_Tim5_Init(void)
{


    CLK_PeripheralClockConfig(CLK_Peripheral_TIM5,ENABLE);//将主时钟信号送给定时器4(L系列单片机必需)
    TIM5_TimeBaseInit(TIM5_Prescaler_16,TIM5_CounterMode_Up,SPOKE_PART_TIMER); //200us
    TIM5_SetCounter(0); 
    TIM5_ITConfig(TIM5_IT_Update,ENABLE);
    TIM5_ARRPreloadConfig(ENABLE);


}



void User_TIM3_For_PWM(void)
{
  TIM3_DeInit();
  //50% pwm 
  // 16M  / 25K  =640
  TIM3_TimeBaseInit(TIM3_Prescaler_1,TIM3_CounterMode_Up,640);
  /*
  */
  /* Toggle Mode configuration: Channel2 */
  TIM3_OC2Init(TIM3_OCMode_PWM2,TIM3_OutputState_Enable,0,TIM3_OCPolarity_Low,TIM3_OCIdleState_Set);
 // TIM1_OC3Init(TIM3_OCMode_PWM2, TIM3_OutputState_Enable, TIM1_OutputNState_Disable,
  //             CCR3_Val, TIM1_OCPolarity_Low, TIM1_OCNPolarity_Low, TIM1_OCIdleState_Set,
  //             TIM1_OCNIdleState_Set);

  TIM3_ARRPreloadConfig(ENABLE);

  /* TIM3 Interrupt enable */
 // TIM1_ITConfig(TIM1_IT_CC3, ENABLE);
 // enableInterrupts();
  /* Enable TIM3 outputs */
  TIM3_CtrlPWMOutputs(ENABLE);
  /* TIM3 enable counter */
  TIM3_Cmd(ENABLE);
}


void User_Set_PWM_Value(uint16_t value)
{
    TIM3->CCR2H = (uint8_t)(value >> 8);
    TIM3->CCR2L = (uint8_t)(value);
}






void delayms_time2(uint16_t n)
{

	int i;
	for(i=0;i<n;i++)
	{
		TIM2_SetCounter(0);
        	Time2_count=0;
		while(Time2_count<=4000)
                {
                    Time2_count = TIM2_GetCounter();
                }

	}
}


void delayms_time4(uint16_t n)
{
    uint16_t x;
    int i= 0;
    for(i=0 ; i< n/1000;i++)
        {
    	TIM4_SetCounter(0);
        while(TIM4_GetCounter() < 1000);
        }
      	TIM4_SetCounter(0);
        while(TIM4_GetCounter() < n%1000);          

}




void delayus_t3(uint16_t n)
{
	int i;
    for(i=0;i<n;i++)
    {
        TIM3_SetCounter(0);
        Time3_count=0;
        while(Time3_count<=4)
        {
        Time3_count = TIM3_GetCounter();
        }

	}
}




void Delay_Ms(uint32_t ms)
{

	__IO uint32_t old_tick=0;

	old_tick = System_Tick;

	while(System_Tick-old_tick < ms)
		{

			;// RESET WDOG

		}




}





