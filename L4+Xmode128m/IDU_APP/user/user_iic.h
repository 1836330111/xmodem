
#ifndef __USER_IIC_H__
#define __USER_IIC_H__








#define sEE_I2C                          I2C1
#define sEE_I2C_CLK                      CLK_Peripheral_I2C1
#define sEE_I2C_SCL_PIN                  GPIO_Pin_1                  /* PC.01 */
#define sEE_I2C_SCL_GPIO_PORT            GPIOC                       /* GPIOC */
#define sEE_I2C_SDA_PIN                  GPIO_Pin_0                  /* PC.00 */
#define sEE_I2C_SDA_GPIO_PORT            GPIOC                       /* GPIOC */
#define sEE_M24C64_32

#define sEE_I2C_DMA                      DMA1
#define sEE_I2C_DMA_CHANNEL_TX           DMA1_Channel3
#define sEE_I2C_DMA_CHANNEL_RX           DMA1_Channel0
#define sEE_I2C_DMA_FLAG_TX_TC           DMA1_FLAG_TC3
#define sEE_I2C_DMA_FLAG_RX_TC           DMA1_FLAG_TC0
#define sEE_I2C_DR_Address               ((uint16_t)0x005216)
#define sEE_USE_DMA

#define sEE_DIRECTION_TX                 0
#define sEE_DIRECTION_RX                 1

#define I2C_SPEED              20000
#define I2C_SLAVE_ADDRESS7    0xa0  // 0xaa

#define sEE_TIMEOUT_MAX         0x10000

#define sEE_HW_ADDRESS	   0x66   















#endif




