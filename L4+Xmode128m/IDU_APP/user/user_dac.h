
#ifndef __USER_ADC_H__
#define __USER_ADC_H__





#include "stm8l15x.h"
#include "stm8l15x_dac.h"



#define BUFFER_SIZE            ((uint8_t) 0x0A)
#define ADC1_DR_ADDRESS        ((uint16_t)0x5344)
#define BUFFER_ADDRESS         ((uint16_t)(&adc_Buffer[0]))



extern uint16_t adc_Buffer[] ;


void Adc_Config(void);


uint16_t Get_Adc_Value(void);
//uint8_t Get_Current(uint8_t color);



void Adc_DMA_Config(void);





#endif




