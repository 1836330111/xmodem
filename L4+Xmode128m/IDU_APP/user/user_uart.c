

#include "public.h"



#define def_baudrate 115200
#define def_wordlen  USART_WordLength_8b
#define def_stopbit  USART_StopBits_1
#define def_parity   USART_Parity_No  







#define PRINT_USART1_ON   1
#define PRINT_USART2_ON   0
#define PRINT_USART3_ON   0



int fputc(int ch, FILE *f)//printf
{
	USART_SendData8(USART1, (uint8_t) ch);

	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {}	
   
    return ch;
}


void User_Uart1_Int(int baud_rate_enum)
{

	uint32_t baud=0;
	

	if(	B115200 == baud_rate_enum)
		{
		baud = 115200;
		}
	else if(B57600 == baud_rate_enum)
		{
		baud = 57600;
		}
	else if(B38400 == baud_rate_enum)
		{
		baud = 38400;
		}
	else if(B28800 == baud_rate_enum)
		{
		baud = 28800;
		}
	else if(B19200 == baud_rate_enum)
		{
		baud = 19200;
		}
	else if(B9600 == baud_rate_enum)
		{
		baud = 9600;
		}
	else
		{
		baud = 115200;
		}


	//initialize UART_RX/TX
	GPIO_Init(GPIOC , GPIO_Pin_2 , GPIO_Mode_In_FL_No_IT);          //RX
	GPIO_Init(GPIOC , GPIO_Pin_3 , GPIO_Mode_Out_PP_Low_Fast);   //TX

	CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);

	USART_Init(USART1 , baud , def_wordlen , def_stopbit , def_parity ,(USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
	ITC_SetSoftwarePriority(USART1_RX_TIM5_CC_IRQn, ITC_PriorityLevel_2);//设置中断优先级 
	USART_ITConfig (USART1,USART_IT_RXNE,ENABLE);//使能接收中断


	USART_DMACmd(USART1, USART_DMAReq_TX, ENABLE); 
	USART_Cmd (USART1,ENABLE);//使能USART	

	

}



void User_Uart2_Int(int baud_rate_enum)
{

	uint32_t baud=0;
	

	if(	B115200 == baud_rate_enum)
		{
		baud = 115200;
		}
	else if(B57600 == baud_rate_enum)
		{
		baud = 57600;
		}
	else if(B38400 == baud_rate_enum)
		{
		baud = 38400;
		}
	else if(B28800 == baud_rate_enum)
		{
		baud = 28800;
		}
	else if(B19200 == baud_rate_enum)
		{
		baud = 19200;
		}
	else if(B9600 == baud_rate_enum)
		{
		baud = 9600;
		}
	else
		{
		baud = 115200;
		}



	GPIO_Init(GPIOE , GPIO_Pin_3 , GPIO_Mode_In_FL_No_IT);          //RX
	GPIO_Init(GPIOE , GPIO_Pin_4 , GPIO_Mode_Out_PP_Low_Fast);   //TX
	
	CLK_PeripheralClockConfig(CLK_Peripheral_USART2, ENABLE);
	USART_Init(USART2 , baud , def_wordlen , def_stopbit , def_parity ,(USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
	USART_ITConfig (USART2,USART_IT_RXNE,ENABLE);//使能接收中断
	USART_Cmd (USART2,ENABLE);//使能USART	


	

}

void User_Uart3_Int(int baud_rate_enum)
{
	uint32_t baud=0;
	

	if(	B115200 == baud_rate_enum)
		{
		baud = 115200;
		}
	else if(B57600 == baud_rate_enum)
		{
		baud = 57600;
		}
	else if(B38400 == baud_rate_enum)
		{
		baud = 38400;
		}
	else if(B28800 == baud_rate_enum)
		{
		baud = 28800;
		}
	else if(B19200 == baud_rate_enum)
		{
		baud = 19200;
		}
	else if(B9600 == baud_rate_enum)
		{
		baud = 9600;
		}
	else
		{
		baud = 115200;
		}


	
	GPIO_Init(GPIOE , GPIO_Pin_7 , GPIO_Mode_In_FL_No_IT);          //RX
	GPIO_Init(GPIOE , GPIO_Pin_6 , GPIO_Mode_Out_PP_Low_Fast);      //TX
	
	CLK_PeripheralClockConfig(CLK_Peripheral_USART3, ENABLE);
	USART_Init(USART3 , baud , def_wordlen , def_stopbit , def_parity ,(USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
	USART_ITConfig (USART3,USART_IT_RXNE,ENABLE);//使能接收中断
	USART_Cmd (USART3,ENABLE);//使能USART		

}



int putchar(int ch)
{
  	uint8_t temp;
	temp=(uint8_t)ch;

	#if   PRINT_USART1_ON==1
	USART1->DR = temp;
  	while(USART_GetFlagStatus(USART1,USART_FLAG_TXE) == RESET);
	#endif
	
	#if   PRINT_USART2_ON==1
	USART2->DR = temp;
  	while(USART_GetFlagStatus(USART2,USART_FLAG_TXE) == RESET);
	#endif
	
 	#if   PRINT_USART3_ON==1
	USART3->DR = temp;
  	while(USART_GetFlagStatus(USART3,USART_FLAG_TXE) == RESET);
	#endif


	return ch;
}






int Uart_Send_Buf(USART_TypeDef * Port,uint8_t* buf,int16_t len)
{
	int i=0;

	for(i=0;i<len;i++)
	{
		Port->DR = buf[i];
		while(USART_GetFlagStatus(Port,USART_FLAG_TXE) == RESET);
	}
	//DMA
	//XXXXXXXXXXXXXXXX

}






/*int E6_Cmd_Send(USART_TypeDef * Port,uint8_t Target,uint8_t* buf,uint8_t len)
{
    uint8_t  head_byte[3]={0xeb,0x90,0x00};
    uint8_t  end_byte=0;
    

    head_byte[2] = Target ;
    

    Uart_Send_Buf(Port,head_byte,3);
    Uart_Send_Buf(Port,&len,1);
    Uart_Send_Buf(Port,buf,len);


    //sum
    end_byte+=head_byte[0];
    end_byte+=head_byte[1];
    end_byte+=head_byte[2];

    end_byte+=len;
    for(int i=0;i<len;i++)
        {
            end_byte+=buf[i];
        }

    
    Uart_Send_Buf(Port,&end_byte,1);

    return len;

}*/




void print_buf(uint8_t* buf,int len)
{

	int i=0;
	for(i=0;i<len;i++)
	{
		printf("%02X ",buf[i]);
		//if(i%16==15)printf("\r\n");
	}
	printf("\r\n");
}











