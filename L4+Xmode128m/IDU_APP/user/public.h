#ifdef ROOT
#define ext 
#else
#define ext extern
#endif



#define SPOKE_PART_TIMER   180 //��λus



#define UART_ADDR_DLP   0X1
#define UART_ADDR_PMU	0X2
#define UART_ADDR_IDU	0X4
#define UART_ADDR_PC	0X8	


#ifndef     __IO
#define     __IO    volatile         /*!< defines 'read / write' permissions  */
#endif




#include "stm8l151c8.h"
#include "stm8l15x_it.h"
#include "stm8l15x.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdarg.h"
#include "system_config.h"
#include "do_message.h"
#include "ir_nec.h"
#include "IS23418.h"

#include "decode.h"
#include "user_timer.h"
#include "ppfifo.h"
//#include "user_adc.h"

#include "user_dac.h"
#include "user_iic.h"
#include "stm8l15x_gpio.h"
#include "user_uart.h"
#include"mcp4131.h"


