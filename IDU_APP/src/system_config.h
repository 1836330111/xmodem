
#ifndef __SYSTEM_CONFIG__
#define __SYSTEM_CONFIG__





#define ROUTE_PORT_DLP   USART3
#define ROUTE_PORT_IDU   USART2
#define ROUTE_PORT_PC    USART1
//#define ROUTE_PORT_PMU   USART1




#define TO_ADDR_DLP    0X10
#define TO_ADDR_PMU	   0X20
#define TO_ADDR_IDU	   0X40
#define TO_ADDR_PC	   0X80	


#define FROM_ADDR_DLP   0X01
#define FROM_ADDR_PMU	0X02
#define FROM_ADDR_IDU	0X04
#define FROM_ADDR_PC	0X08	




typedef struct _SYSTEM_CONFIG
{
    
	uint8_t Dlp_Uarst_Bypass;//0关闭透传, 1 开启DLP串口透传

	uint8_t Pmu_Baud_Rate_Enum;
    uint8_t Standby_Mode;  //0:正常待机 1: 低功耗 待机
    uint8_t Power_On_Mode;//   1.上电开机  0.上电待机
    


}SYSTEM_CONFIG,*pSYSTEM_CONFIG;





extern SYSTEM_CONFIG g_System_Config;








void SYSTEM_Config_Init(void);





int  Load_System_Config(void);



int  Save_System_Config(void);


#endif










