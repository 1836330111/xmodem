

#ifndef __STM8_L151C8_H__
#define __STM8_L151C8_H__

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "stm8l15x.h"
#include "user_uart.h"

#include "system_config.h"
#include "user_timer.h"
#include "decode.h"
#include "ppfifo.h"
#include "user_dac.h"
#include "IS23418.h"



//////////////////////////////////////////////////////

#define MAJOR_VERSION   1
#define SUBEDITION      1
#define def_baudrate    9600
#define def_wordlen     USART_WordLength_8b
#define def_stopbit     USART_StopBits_1
#define def_parity      USART_Parity_No

#define USE_USART1      0

#define UART1_RECV_REQ			0X01
#define UART1_SEND_REQ			0X02
#define LASER_ENABLE_REQ		0X04
#define UART_BUFF_SIZE			16

#define TIM4_Period     29
//#define TIM2_Period     0xFFFF
#define TIM3_Period     0xFFFF
#define TIM5_Period     0xFFFF
#define TIM1_PERIOD             0x140
#define TIM1_PRESCALER              0x01
#define TIM1_REPTETION_COUNTER      0x00



//#define BUFFER_ADDRESS_DAC     ((uint8_t)(&RGBY_data[0]))

#define FAN_SPEED_STOP     0x00
#define FAN_SPEED_SLOW     (TIM1_PERIOD/10)*2
#define FAN_SPEED_MIDDLE   (TIM1_PERIOD/10)*5
#define FAN_SPEED_HIGH      TIM1_PERIOD

#define LMT(x,y,z)  ( ( (y) < (x) ) ? (x) : ( ( (y) > (z) ) ? (z) : (y) ) )
#define MIN(x,y)   ( ( (x) <= (y) ) ? (x) : (y) )

#define RGBY_DATA_MAX			127
#define RGBY_DATA_MIN			5




typedef enum {

    LIWEI=0,
    MAIGE=1,


}e_device_version_t;




#define FILTER_N 52
typedef struct
{
	uint8_t index;
	uint8_t Temp;
}Temp_Table;


typedef enum
{
	TEMP_TO_SHUDOWN,
		
	TEMP_TO_NORMAL,
	
	TEMP_TO_DEFAULT,
}TEMP_MODE;

typedef enum
{	
	IDU_CURRENT_R = 0x01,
	
	IDU_CURRENT_G = 0x02,
	
	IDU_CURRENT_B = 0x03,
	
	IDU_CURRENT_Y = 0x04,

	IDU_CURRENT_C = 0x05,

	IDU_CURRENT_M = 0x06,

	IDU_CURRENT_W = 0x07,

	CW_INDEX_DELAY_CMD = 0x08,
	
	IDU_VER_CMD = 0x20,
		
	IDU_ONLINE_CHK_CMD = 0x51,
	
	IDU_TEMP_CHK_CMD = 0x0b,
	
	IDU_FANSPEED_CHK_CMD = 0x0c,
	
	IDU_FANSWITCH    = 0x0d,
	
	IDU_FANSPEED_ERR_CMD = 0x91,
	
	IDU_TEMP_ERR_CMD = 0x90,

	IDU_FANSPEED_SET_CMD = 0x10,

	IDU_SPOKE_CTR = 0x11, //idu  spoke 开关
    IDU_SPOKE_DELAY = 0x12, //idu  spoke每一段SPOKE 时间


    UPDATE_APP_CMD = 0x3A
    
} IDU_USART1_CMD;

typedef  enum
{
	TARGET_HOST = 0x0A,  

	TARGET_DP9320,

	TARGET_IDU,

	TARGET_DDP,

	TARGET_DDP2,

	TARGET_STM32,

} COMM_TYPE;










////////////////////////////////////////////////////////////////

extern uint8_t Do_Recv_Cmd[20];






//////////////////////base/////////////////////////////////
extern pppfifo_t g_pppfifo;

/////////////////////////////////////////


void Hardware_Init(void);
void Date_Time_Int(void);




#define CHECK_LD_EN  ((GPIOD->IDR&GPIO_Pin_3)?(1):(0))



#define PG_OK   {GPIOD->ODR |= GPIO_Pin_1;}
#define PG_NG   {GPIOD->ODR &=(~GPIO_Pin_1);}



//#define SPOKE_ON      {GPIOD->ODR |= GPIO_Pin_5;}
//#define SPOKE_OFF     {GPIOD->ODR &=(~GPIO_Pin_5);}

#define SPOKE_ON      {GPIOD->ODR |= GPIO_Pin_5;}
#define SPOKE_OFF     {GPIOD->ODR &=(~GPIO_Pin_5);}





#define LARSE_ON      {GPIOD->ODR &=(~GPIO_Pin_2);}
#define LARSE_OFF     {GPIOD->ODR |= GPIO_Pin_2;}



#define LED_RED_ON      GPIO_SetBits(GPIOA, GPIO_Pin_3)  //{GPIOA->ODR |= GPIO_Pin_3;}
#define LED_RED_OFF     GPIO_ResetBits(GPIOA, GPIO_Pin_3)  //{GPIOA->ODR &=(~GPIO_Pin_3);}



extern uint8_t g_device_version; 



#endif


