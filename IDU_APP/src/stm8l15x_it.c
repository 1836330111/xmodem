/**
  ******************************************************************************
  * @file    Project/STM8L15x_StdPeriph_Template/stm8l15x_it.c
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    28-June-2013
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all peripherals interrupt service routine.
  ******************************************************************************
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/


#include "public.h"
#include "do_message.h"


#define TIM_RGB  TIM5

volatile int spoke_param;
volatile int B_Temp;
//extern uint8_t g_Segment_Preset_Cmd;//分段电流预设控制变量
//extern uint8_t g_SPOKE_OFF_Cmd;//结束spoke段控制变量1:spoke off   0:spoke on
//extern volatile uint8_t g_RGB_Present;//当前色段

//#define reliable_test

//static int timer_cnt=0;

    
/** @addtogroup STM8L15x_StdPeriph_Template
  * @{
  */
	
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/

#ifdef _COSMIC_
/**
  * @brief Dummy interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT_HANDLER(NonHandledInterrupt,0)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
#endif

/**
  * @brief TRAP interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief FLASH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(FLASH_IRQHandler,1)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */

	
}
/**
  * @brief DMA1 channel0 and channel1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler,2)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
/**
  * @brief DMA1 channel2 and channel3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler,3)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		//sEE_I2C_DMA_TX_IRQHandler();

    
}
/**
  * @brief RTC / CSS_LSE Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler,4)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief External IT PORTE/F and PVD Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTB / PORTG Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTIB_G_IRQHandler,6)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTD /PORTH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTID_H_IRQHandler,7)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN0 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI0_IRQHandler,8)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
    EXTI_ClearITPendingBit(EXTI_IT_Pin0);
}

/**
  * @brief External IT PIN1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI1_IRQHandler,9)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
   // IR_NEC_KEY_ISR();
	EXTI_ClearITPendingBit(EXTI_IT_Pin1);
}

/**
  * @brief External IT PIN2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */



	//清除中断标志
	EXTI->SR1 = 0x04;
    //mmEXTI_ClearITPendingBit(EXTI_IT_Pin2);
}

/**
  * @brief External IT PIN3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */

//CW_B
    uint8_t index=0;
    uint8_t spoke_delay_index=0;

    EXTI->SR1 = EXTI_IT_Pin3;
    //g_RGB_Present=SEGMENT_Y;

	TIM3->CNTRH = 0;
  	TIM3->CNTRL = 0;
    
    if(g_cw_start_color == CW_B)
        {
            g_index = g_start_index;
        }
     
    g_index++;

	current_update_flag = 1;
    index = g_index % g_cw_segments_cnt;
	if(Dynamic_contrast_Flag == 0)
	{
	   g_active_curren_value = g_resistance_value[index];
	}
   
	g_current_color = g_cw_mode[g_index % g_cw_segments_cnt];
    spoke_delay_index = (g_index - 1) % g_cw_segments_cnt;
    
    if((Spoke_OnOff==1)&&(g_cw_spoke_time[spoke_delay_index] > 50))
        {
        
        TIM5->ARRH = (uint8_t)(g_cw_spoke_time[spoke_delay_index] >> 8);
        TIM5->ARRL = (uint8_t)(g_cw_spoke_time[spoke_delay_index]);       
        TIM5->CR1 |= TIM_CR1_CEN;
        SPOKE_ON;
       
        }

#if 0
    if(g_uart_delay_count<100)
        {
       g_uart_delay_count++;
        }
#endif


}

/**
  * @brief External IT PIN4 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI4_IRQHandler,12)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */


        // CW_G
	uint16_t interval_int = 0;
    uint8_t index=0;
    uint8_t spoke_delay_index=0;
    EXTI->SR1 = 0x10;

	interval_int = ((uint16_t)TIM3->CNTRH << 8) | ((uint16_t)TIM3->CNTRL);		
	TIM3->CNTRH = 0;
	TIM3->CNTRL = 0;
	
	if ( interval_int < 30)
	{		 
		 return;
	}
 
    if(g_cw_start_color == CW_G)
        {
            g_index = g_start_index;
        }
   

    g_index++;

	current_update_flag = 1;
    index = g_index % g_cw_segments_cnt;
    if(Dynamic_contrast_Flag == 0)
	{
	   g_active_curren_value = g_resistance_value[index];
	}
	g_current_color = g_cw_mode[g_index % g_cw_segments_cnt];
    spoke_delay_index = (g_index - 1) % g_cw_segments_cnt;
    
    if((Spoke_OnOff==1)&&(g_cw_spoke_time[spoke_delay_index] > 50))
        {
        
        TIM5->ARRH = (uint8_t)(g_cw_spoke_time[spoke_delay_index] >> 8);
        TIM5->ARRL = (uint8_t)(g_cw_spoke_time[spoke_delay_index]);       
        TIM5->CR1 |= TIM_CR1_CEN;
        SPOKE_ON;
       
        }



}

/**
  * @brief External IT PIN5 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI5_IRQHandler,13)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
    //CW_R
    uint16_t interval_int = 0;
    uint8_t index=0;
    uint8_t spoke_delay_index=0;
    //GPIOD->ODR |= 0xA0;   
    EXTI->SR1 = 0x20;  
	
	interval_int = ((uint16_t)TIM3->CNTRH << 8) | ((uint16_t)TIM3->CNTRL);		
	TIM3->CNTRH = 0;
	TIM3->CNTRL = 0;
	if ( interval_int < 30)
	{		 
		 return;
	}

    if(g_cw_start_color == CW_R)
        {
            g_index = g_start_index;
        }


    g_index++;

	current_update_flag = 1;
    index = g_index % g_cw_segments_cnt;
    if(Dynamic_contrast_Flag == 0)
	{
	   g_active_curren_value = g_resistance_value[index];
	}
	g_current_color = g_cw_mode[g_index % g_cw_segments_cnt];
    spoke_delay_index = (g_index - 1) % g_cw_segments_cnt;
    
    if((Spoke_OnOff==1)&&(g_cw_spoke_time[spoke_delay_index] > 50))
        {
        
        TIM5->ARRH = (uint8_t)(g_cw_spoke_time[spoke_delay_index] >> 8);
        TIM5->ARRL = (uint8_t)(g_cw_spoke_time[spoke_delay_index]);       
        TIM5->CR1 |= TIM_CR1_CEN;
        SPOKE_ON;
       
        }





}

/**
  * @brief External IT PIN6 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI6_IRQHandler,14)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
    EXTI_ClearITPendingBit(EXTI_IT_Pin6);
}

/**
  * @brief External IT PIN7 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
    EXTI_ClearITPendingBit(EXTI_IT_Pin7);
}
/**
  * @brief LCD /AES Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(LCD_AES_IRQHandler,16)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief CLK switch/CSS/TIM1 break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler,17)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief ADC1/Comparator Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(ADC1_COMP_IRQHandler,18)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief TIM2 Update/Overflow/Trigger/Break /USART2 TX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
{
    //SPOKE_ON;GPIOD->ODR |= GPIO_Pin_5
    GPIOD->ODR ^= GPIO_Pin_5;
	TIM2_ClearITPendingBit(TIM2_IT_Update);
    

}

/**
  * @brief Timer2 Capture/Compare / USART2 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler,20)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */

	unsigned char temp=0;
	USART_ClearITPendingBit(USART2,USART_IT_RXNE); 
	temp =  USART_ReceiveData8(USART2);
	//ppfifo_in(g_pppfifo,&temp,1);
	//USART1->DR = temp;


	
}


/**
  * @brief Timer3 Update/Overflow/Trigger/Break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
	//清中断标志位
	#ifdef  TIM_RGB==TIM3 
	TIM3_ClearFlag((uint16_t)0x0001); 

	//TIM3_ClearFlag((uint16_t)0x0001); 
	
	//关spoke
	SPOKE_OFF;

	//置位变量
	//g_SPOKE_OFF_Cmd = 1;

	//关闭TIM_RGB中断
	TIM_RGB->CR1 &= (uint8_t)(~TIM_CR1_CEN);
	#endif
	
	//TIM3->SR1 = (~(uint8_t)TIM3_IT_Update);
    //TIM3_ClearITPendingBit(TIM3_IT_Update);


	
   // TIM3_ClearITPendingBit(TIM3_IT_Update);
	//GPIO_ToggleBits(GPIOA,GPIO_Pin_2);

}
/**
  * @brief Timer3 Capture/Compare /USART3 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler,22)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */

	unsigned char temp=0;
	USART_ClearITPendingBit(USART3,USART_IT_RXNE); 
	temp =  USART_ReceiveData8(USART3);
	//ppfifo_in(g_pppfifo,&temp,1);

}
/**
  * @brief TIM1 Update/Overflow/Trigger/Commutation Interrupt routine.
  * @param  None
  * @retval None
  */

uint16_t i = 0;

INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler,23)
{

	//i++;
	TIM1_ClearITPendingBit(TIM1_IT_Update);

    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief TIM1 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */

uint16_t pwm_value = 1.0;

uint16_t pwm_total = 0;
uint16_t pwm_count = 0;





INTERRUPT_HANDLER(TIM1_CC_IRQHandler,24)
{
    static uint8_t flag;
	static uint16_t tmpccr3 = 0;
	static uint16_t tmpccr3l = 0;
        static uint16_t tmpccr3h = 0;
	
	
#if 1

  //if(TIM1_GetITStatus(TIM1_IT_CC3) != RESET) //如果CH3边沿触发
  if(TIM1->SR1&TIM1_IT_CC3) //如果CH3边沿触发
  {
    TIM1->SR1 = (uint8_t)(~(uint8_t)TIM1_IT_CC3);
	tmpccr3h = TIM1->CCR3H ;
	tmpccr3l = TIM1->CCR3L ;
	TIM1->CNTRH=0;
	TIM1->CNTRL=0;
	//TIM1->CNTRH=0;
	//TIM1->CNTRL=0;

	//tmpccr3 = (uint16_t)(tmpccr3l);

	//tmpccr3 |= ((uint16_t)tmpccr3h << 8);
        
        tmpccr3 = tmpccr3l;
     #if 0  
	if(index_pwm >= MAX_PWM_SIZE)
		{
			index_pwm = 0;
		}


	if (flag != TIM1_ICPolarity_Rising)
	{
		flag  = TIM1_ICPolarity_Rising;
		TIM1->CCER2 |= TIM1_CCER2_CC3P;
		pwm_buf_0[index_pwm] =  tmpccr3;
	}
	else
	{
		flag  = TIM1_ICPolarity_Falling;
		TIM1->CCER2 &= (uint8_t)(~TIM1_CCER2_CC3P);
		pwm_buf_1[index_pwm++] =  tmpccr3;
	}
  #endif

  #if 1  
  	if(index_pwm >= MAX_PWM_SIZE)
		{
			index_pwm = 0;
		}

	if (flag != TIM1_ICPolarity_Rising)
	{
            flag  = TIM1_ICPolarity_Rising;
            TIM1->CCER2 |= TIM1_CCER2_CC3P;
            pwm_total +=  tmpccr3;
            pwm_count++;
            if(pwm_count >= 100)
            {
               PwmBufL = pwm_total/pwm_count;
               pwm_count = 0;
               pwm_total = 0;
            }
		
	}
	else
	{
		flag  = TIM1_ICPolarity_Falling;
		TIM1->CCER2 &= (uint8_t)(~TIM1_CCER2_CC3P);
		//pwm_buf_1[index_pwm++] =  tmpccr3;
	}


  #endif

	// TIM1_GetCapture3();

	//TIM1_ClearITPendingBit(TIM1_IT_CC3);
  }

  #endif
 // TIM1_ClearITPendingBit(TIM1_IT_CC3);
  //TIM1->SR1=0x00;
}

/**
  * @brief TIM4 Update/Overflow/Trigger Interrupt routine.
  * @param  None
  * @retval None
  */
  
extern  __IO uint32_t System_Tick;
 INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler,25)
{
	
	System_Tick++;
	//TIM4->SR1 = (uint8_t)(~(uint8_t)TIM4_IT_Update);
    //GPIOD->ODR ^= GPIO_Pin_5;

	TIM4_ClearITPendingBit(TIM4_IT_Update);
	//TIM4_ClearITPendingBit(TIM4_IT_Update);
  
}
/**
  * @brief SPI1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(SPI1_IRQHandler,26)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */		
}

/**
  * @brief USART1 TX / TIM5 Update/Overflow/Trigger/Break Interrupt  routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler,27)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
     //   GPIOD->ODR ^= GPIO_Pin_5;
    SPOKE_OFF;
	TIM5_ClearFlag(TIM5_FLAG_Update); 

#ifdef 0 //TIM_RGB==TIM5 
	TIM5_ClearFlag(TIM5_FLAG_Update); 

	//关spoke
	SPOKE_OFF;

	//置位变量
	//g_SPOKE_OFF_Cmd = 1;

	//关闭TIM_RGB中断
	TIM_RGB->CR1 &= (uint8_t)(~TIM_CR1_CEN);
#endif




}

/**
  * @brief USART1 RX / Timer5 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
{


	//static uint8_t index=0;
	//USART_ClearITPendingBit(USART1,USART_IT_TC);
	uint8_t temp = 0;

	temp = USART1->DR;
	//USART1->SR &= (uint8_t)(~USART_SR_TC);
    USART1->SR &= (uint8_t)(~USART_IT_RXNE);
    ppfifo_in(Decode_Lx.pf,&temp,1);

   // USART_ClearITPendingBit(USART1,USART_IT_RXNE);
	




}




#if 0


INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
{


    static uint8_t index=0;
    //USART_ClearITPendingBit(USART1,USART_IT_TC);
    uint8_t temp = 0;

    temp = USART1->DR;
    USART1->SR &= (uint8_t)(~USART_SR_TC);


    if(g_uart_delay_count > 80) 
    {
        index = 0; 
    }


    if(index < 16)    
    {
        UartRx_buff[index++] = temp ;
        if((index >= 16)&&(UartRx_buff[0] == 0xA5)&&(UartRx_buff[15] == 0xA5))
        {
        Laser_cmd_req |=UART1_RECV_REQ;
        index = 0;
            for(int i=0; i<16;i++)
            {
            Do_Recv_Cmd[i] =  UartRx_buff[i];

            }
        }
    } 
    g_uart_delay_count = 0;


}



INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
{
#if 0
	unsigned char temp=0;
	USART_ClearITPendingBit(USART1,USART_IT_RXNE); 
	temp =  USART_ReceiveData8(USART1);
#endif


    static uint8_t index=0;
    //USART_ClearITPendingBit(USART1,USART_IT_TC);

    

    USART1->SR &= (uint8_t)(~USART_SR_TC);
    if((Laser_cmd_req  & UART1_RECV_REQ)==0)
    {
      if(g_uart_delay_count > 40) 
        {
        index = 0; 
        }
      if(index < 16)    
      {
              UartRx_buff[index++] = USART1->DR;//USART_ReceiveData8(USART1);
              
              if((index >= 16)&&(UartRx_buff[0] == 0xA5)&&(UartRx_buff[15] == 0xA5))
              {
                  Laser_cmd_req |=UART1_RECV_REQ;
                  index = 0;
                  for(int i=0; i<16;i++)
                    {
                        Do_Recv_Cmd[i] =  UartRx_buff[i];

                    }
              }
      } 
      g_uart_delay_count = 0;
    }
    else 
    {
        USART1->DR;//USART_ReceiveData8(USART1);
    }

}


#endif

/**
  * @brief I2C1 / SPI2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler,29)
{
  
}


/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
