#include "public.h"


void Mcp4131_SPI_INIT(void)
{
	/* Enable SPI clock */
	CLK_PeripheralClockConfig(LD_SPI_CLK, ENABLE);


	/* Set the MOSI,MISO and SCK at high level */
	GPIO_ExternalPullUpConfig(LD_SPI_PIN,  LD_SPI_MISO_PIN | LD_SPI_MOSI_PIN | \
	            LD_SPI_SCK_PIN, ENABLE);

//#if(BOARD_SPI_VERION == SPI_MCP4131)
	GPIO_SetBits(GPIOB ,LD_SPI_CS_1);
	GPIO_SetBits(GPIOF ,LD_SPI_CS_2);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_3);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_4);
	/* SD_SPI Config */
	SPI_Init(LD_SPI, SPI_FirstBit_MSB, SPI_BaudRatePrescaler_16, SPI_Mode_Master,
	SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_Direction_2Lines_FullDuplex,
	SPI_NSS_Soft, 0x07);
/*#else
	GPIO_SetBits(GPIOF ,LD_SPI_CS_1);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_2);
	GPIO_SetBits(GPIOB ,LD_SPI_CS_3);
	GPIO_SetBits(GPIOD ,LD_SPI_CS_4);
	SPI_Init(LD_SPI, SPI_FirstBit_MSB, SPI_BaudRatePrescaler_4, SPI_Mode_Master,
	SPI_CPOL_High, SPI_CPHA_2Edge, SPI_Direction_2Lines_FullDuplex,
	SPI_NSS_Soft, 0x07);*/
//#endif


	/* SD_SPI enable */
	SPI_Cmd(LD_SPI, ENABLE);

  
}



void MCP4131_WriteCurrent(uint8_t data)
{

	uint8_t send[2] = {0};
	//GPIO_ResetBits(GPIOB,LD_SPI_CS_1);
	//GPIO_ResetBits(GPIOF ,LD_SPI_CS_2);
	//GPIO_ResetBits(GPIOD ,LD_SPI_CS_3);

	GPIOB->ODR &= (uint8_t)(~LD_SPI_CS_1);
	 GPIOF->ODR &= (uint8_t)(~LD_SPI_CS_2);
	 GPIOD->ODR &= (uint8_t)(~LD_SPI_CS_3);
	 GPIOD->ODR &= (uint8_t)(~LD_SPI_CS_4);

	 
	nop();
	send[0] = (0&0xF0)|(0&0x0C);//|((uint8_t)(data>>8)&0x01);
	send[1] = (data&0xFF);
	SPI_SENDREADByte(send[0]);
       SPI_SENDREADByte(send[1]);
	nop();

	GPIOB->ODR |= LD_SPI_CS_1;
	GPIOF->ODR |= LD_SPI_CS_2;
	GPIOD->ODR |= LD_SPI_CS_3;
	GPIOD->ODR |= LD_SPI_CS_4;
}



void Current_Handle_Value(void)
{
    int i=0;
	static uint16_t pre_active_curren_value;

	if(pre_active_curren_value !=g_active_curren_value)
	{
	    pre_active_curren_value = g_active_curren_value;

        if(pre_active_curren_value <= RGBY_DATA_MIN)
            {
            
            	MCP4131_WriteCurrent(RGBY_DATA_MIN);

            }
        else if(pre_active_curren_value >= RGBY_DATA_MAX)
            {
            
            	MCP4131_WriteCurrent(RGBY_DATA_MAX);

            }
        else
            {
                MCP4131_WriteCurrent(pre_active_curren_value);
            }
		
	}
	
}


