

#include "public.h"


__IO uint16_t sEEAddress = 0;
__IO uint32_t sEETimeout = sEE_TIMEOUT_MAX;
__IO uint16_t* sEEDataReadPointer;
__IO uint8_t* sEEDataWritePointer;
__IO uint8_t sEEDataNum;

#define i2c_speed       100000
#define i2c_addr        0x66



#if 1


unsigned char check_iic(I2C_Event_TypeDef I2C_Event)
{
	int i=0;
	#if 0
	while (!I2C_CheckEvent(sEE_I2C, I2C_Event))
	{}
	#else
	

	for(i=0;i<5000;i++)
	{
	  if(!I2C_CheckEvent(sEE_I2C, I2C_Event))
	  {
		  ;
	  }
	  else
	  {
		  return 1;
	  }
	}
	#endif

	return 0;

}


void idu_write(unsigned char index,uint8_t ch)
{

  
  /*!< Send STRAT condition */
  I2C_GenerateSTART(sEE_I2C, ENABLE);
  //delayms(1);
  // delayfor(10);

  /*!< Test on EV5 and clear it */
  
  check_iic(I2C_EVENT_MASTER_MODE_SELECT);


  /*!< Send EEPROM address for write */
  I2C_Send7bitAddress(sEE_I2C, 0x66, I2C_Direction_Transmitter);
  // delayfor(10);
  /*!< Test on EV6 and clear it */
  check_iic(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED);


  /*!< Send the EEPROM's internal address to write to : MSB of the address first */
	I2C_SendData(sEE_I2C, index);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

	I2C_SendData(sEE_I2C, index);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

	I2C_SendData(sEE_I2C, 2);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);
	
	I2C_SendData(sEE_I2C, 3);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);
	
	I2C_SendData(sEE_I2C, ch);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

  /*!< Send STOP condition */
  I2C_GenerateSTOP(sEE_I2C, ENABLE);
}





void sEE_WriteByte(uint8_t* pBuffer, uint16_t WriteAddr)
{

  
  /*!< Send STRAT condition */
  I2C_GenerateSTART(sEE_I2C, ENABLE);
  //delayms(1);
  // delayfor(10);

  /*!< Test on EV5 and clear it */
  
  check_iic(I2C_EVENT_MASTER_MODE_SELECT);


  /*!< Send EEPROM address for write */
  I2C_Send7bitAddress(sEE_I2C, 0x66, I2C_Direction_Transmitter);
  // delayfor(10);
  /*!< Test on EV6 and clear it */
  check_iic(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED);


  /*!< Send the EEPROM's internal address to write to : MSB of the address first */
	I2C_SendData(sEE_I2C, 0x00);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

	I2C_SendData(sEE_I2C, pBuffer[0]);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

	I2C_SendData(sEE_I2C, pBuffer[1]);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);
	
	I2C_SendData(sEE_I2C, pBuffer[2]);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);
	
	I2C_SendData(sEE_I2C, pBuffer[3]);
	check_iic(I2C_EVENT_MASTER_BYTE_TRANSMITTED);

          /*!< Send STOP condition */
        I2C_GenerateSTOP(sEE_I2C, ENABLE);
}



#endif






void sEE_I2C_DMA_TX_IRQHandler(void)
{
  /* Check if the DMA transfer is complete */
  if (DMA_GetFlagStatus(sEE_I2C_DMA_FLAG_TX_TC) != RESET)
  {
    /* Disable the DMA Tx Channel and Clear all its Flags */
    DMA_Cmd(sEE_I2C_DMA_CHANNEL_TX, DISABLE);
    DMA_ClearFlag(sEE_I2C_DMA_FLAG_TX_TC);

    /*!< Wait till all data have been physically transferred on the bus */
    while (!I2C_CheckEvent(sEE_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    {}

    /*!< Send STOP condition */
    I2C_GenerateSTOP(sEE_I2C, ENABLE);

    /* Reset the variable holding the number of data to be written */
    *sEEDataWritePointer = 0;
  }
}


void sEE_LowLevel_Init(void)
{
  /*!< sEE_I2C Periph clock enable */
  CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);

  /*!< Enable the DMA clock */
  CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);

  /* I2C TX DMA Channel configuration */
  DMA_DeInit(sEE_I2C_DMA_CHANNEL_TX);
  DMA_Init(sEE_I2C_DMA_CHANNEL_TX,
           0, /* This parameter will be configured durig communication */
           sEE_I2C_DR_Address,
           0xFF, /* This parameter will be configured durig communication */
           DMA_DIR_PeripheralToMemory,/* This parameter will be configured durig communication */
           DMA_Mode_Normal,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_VeryHigh,
           DMA_MemoryDataSize_Byte);

  /* I2C RX DMA Channel configuration */
  DMA_DeInit(sEE_I2C_DMA_CHANNEL_RX);
  DMA_Init(sEE_I2C_DMA_CHANNEL_RX, 0, /* This parameter will be configured durig communication */
           sEE_I2C_DR_Address,
           0xFF, /* This parameter will be configured durig communication */
           DMA_DIR_PeripheralToMemory,/* This parameter will be configured durig communication */
           DMA_Mode_Normal,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_VeryHigh,
           DMA_MemoryDataSize_Byte);


}


void IIC_Init(void)
{
  //sEE_LowLevel_Init();

  /*!< I2C configuration */
  /* sEE_I2C Peripheral Enable */
  //I2C_Cmd(sEE_I2C, ENABLE);
  /* sEE_I2C configuration after enabling it */
  I2C_Init(sEE_I2C, I2C_SPEED, I2C_SLAVE_ADDRESS7, I2C_Mode_I2C, I2C_DutyCycle_2,
           I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);


  I2C_Cmd(sEE_I2C, ENABLE);

  /* Enable the sEE_I2C peripheral DMA requests */
  //I2C_DMACmd(sEE_I2C, ENABLE);

  sEEAddress = sEE_HW_ADDRESS;

  
}












