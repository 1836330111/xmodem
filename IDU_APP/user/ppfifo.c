#ifdef __cplusplus
extern "C" {
#endif
    
#include "public.h"
  
#pragma pack (1)

pppfifo_t g_pppfifo;


/********************************************************************************************************
*	函数名	:  	ppfifo_t* ppfifo_alloc(unsigned int vsize)
*	功能	   	: 	申请vsize 长度的PP缓冲区
*	返回值	:	pp缓冲区的指针
*	参数 1		:	
*	参数 2 	:	
*	参数 3		:	
*********************************************************************************************************/
ppfifo_t* ppfifo_alloc(unsigned int vsize)
{
    ppfifo_t* ppfifo;
    
    ppfifo=(ppfifo_t*)((unsigned char*)malloc(2 * vsize + sizeof(ppfifo_t)));
        
    if(ppfifo == 0)   
        return 0;
    
    ppfifo->mode = 0;
    ppfifo->buffer_toal = vsize;
    ppfifo->buffer_len_1 = 0;
    ppfifo->buffer_len_2 = 0;
    ppfifo->buffer_1 = sizeof(ppfifo_t) + (unsigned char*)ppfifo;
    ppfifo->buffer_2 = sizeof(ppfifo_t) + (unsigned char*)ppfifo + vsize;
    ppfifo->ppdata = sizeof(ppfifo_t) + (unsigned char*)ppfifo + vsize + vsize ;
       
    memset((unsigned char *)ppfifo->buffer_1,0,vsize);
    memset((unsigned char *)ppfifo->buffer_2,0,vsize);
    memset((unsigned char *)ppfifo->ppdata,0,vsize);
    
    return ppfifo;
}
/********************************************************************************************************
*	函数名	:  	void ppfifo_free(ppfifo_t *ppfifo)
*	功能	   	: 	释放已存在的PP缓冲区指针
*	返回值	:	
*	参数 1		:	
*	参数 2 	:	
*	参数 3		:	
*********************************************************************************************************/

void ppfifo_free(ppfifo_t *ppfifo)
{   
    if(ppfifo)
    {
        free(ppfifo);
    }
}
/********************************************************************************************************
*	函数名	:  	unsigned int ppfifo_in(ppfifo_t *fifo,unsigned char *buf, unsigned int len)
*	功能	   	: 	将len 长度的buf 写入pp缓冲区
*	返回值	:	0，写入失败，其他写入的有效数据长度  
*	参数 1		:	fifo pp缓冲区的指针
*	参数 2 	:	buf 待写入的buf指针
*	参数 3		:	len buf 最大长度
*********************************************************************************************************/

unsigned int ppfifo_in(ppfifo_t *fifo,unsigned char *buf, unsigned int len)
{
    unsigned int i = 0;
    if((fifo == 0)||(buf == 0)||(len == 0))   
        return 0;
    
    if(fifo->mode == 0)
    {
        if((fifo->buffer_toal - fifo->buffer_len_1) < len)
        {
            return 0;
        }
        for(i = 0; i < len; i ++)
        {
            fifo->buffer_1[fifo->buffer_len_1] = buf[i];
            fifo->buffer_len_1 ++;
        }
    }
    else
    {
        if((fifo->buffer_toal - fifo->buffer_len_2) < len)
        {
            return 0;
        }
        
        for(i = 0; i < len; i ++)
        {
            fifo->buffer_2[fifo->buffer_len_2] = buf[i];
            fifo->buffer_len_2 ++;
        }
    }
    
    return len;
}
/********************************************************************************************************
*	函数名	:  	unsigned int vfifo_in(vfifo_t *fifo,unsigned char *buf, unsigned int len)
*	功能	   	: 	将len 长度的buf 取出PP缓冲区
*	返回值	:	0，无数据，其他，取出的有效数据长度  
*	参数 1		:	fifo pp缓冲区的指针
*	参数 2 	:	buf 收取数据的buf指针
*	参数 3		:	l
*********************************************************************************************************/

unsigned int ppfifo_out(ppfifo_t *fifo,unsigned char *buf)
{
    unsigned int i = 0;
    
    if((fifo == 0)||(buf == 0))   //lxb add 2016年5月28日11:35:26
        return 0;
    
    if(fifo->mode == 0)
    {
        fifo->mode = 1;  // 有可能被中断
        for(i = 0; i < fifo->buffer_len_1; i ++)
        {
            buf[i] = fifo->buffer_1[i];
        }
        memset(fifo->buffer_1,0,i);
        fifo->buffer_len_1 = 0;
        
        return i;
    }
    else
    {
        fifo->mode = 0;   // 有可能被中断
        
        for(i = 0; i < fifo->buffer_len_2; i ++)
        {
            buf[i] = fifo->buffer_2[i];
        }
        memset(fifo->buffer_2,0,i);
        fifo->buffer_len_2 = 0;
        
        return i;
    }		
}


#pragma pack ()

#ifdef __cplusplus
}
#endif
