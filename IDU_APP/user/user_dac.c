

#include "public.h"



uint16_t adc_Buffer[BUFFER_SIZE];

#if 0
struct resistance_current{
    uint8_t resistance;
    float   current;
    uint16_t adc_value;
};

const struct resistance_current res_table[] = {
    {80, 0.9, 350},
    {51, 1.2, 780},
    {36, 1.5, 1150},
    {25, 1.8, 1650},
    {18, 2.1, 2050},
    {13, 2.4, 2450},
    {10, 2.7, 2850},
    {6, 3.0, 3260},
    {4, 3.3, 3760},
    {2, 3.5, 4095},
};
#endif

void Adc_Config(void) 
{

    /* Initialise and configure ADC1 */
    ADC_DeInit(ADC1);
    ADC_Init(ADC1, ADC_ConversionMode_Continuous, ADC_Resolution_12Bit, ADC_Prescaler_2);
    ADC_SamplingTimeConfig(ADC1, ADC_Group_SlowChannels, ADC_SamplingTime_384Cycles);
    
    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);
    
    /* Enable ADC1 Channel 3 */
    ADC_ChannelCmd(ADC1 , ADC_Channel_0 , ENABLE);
    //ADC_ChannelCmd(ADC1 , ADC_Channel_17 , ENABLE);
    /* Enable End of conversion ADC1 Interrupt */
    ADC_ITConfig(ADC1, ADC_IT_EOC, DISABLE);
    
    /* Start ADC1 Conversion using Software trigger*/
    ADC_SoftwareStartConv(ADC1);


   // Adc_DMA_Config();
    
    ADC_DMACmd(ADC1,ENABLE);

}


#if 0
uint8_t Get_Current(uint8_t color)
{
	uint8_t i;
	uint16_t adc_value;
	const static uint8_t size = sizeof(res_table)/sizeof(struct resistance_current);  //size = 4;
	adc_value = adc_Buffer[color];//color: 0-red, 1-Green, 2-Blue,3-yellow
	//optimized table look-up.
	if(adc_value > res_table[size>>1].adc_value) //size = 4; (size>>1) = 2; (size>>2) = 1; //(adc_value > res_table[2].adc_value)
	{
			if(adc_value > res_table[(size>>1)+(size>>2)].adc_value)  //(adc_value > res_table[3].adc_value)
			{
					for(i = (size>>1)+(size>>2); i <= size; i++) //(i = 3;i<4;i++)
					{
							if(res_table[i-1].adc_value < adc_value && res_table[i].adc_value >= adc_value) 
							{
									return res_table[i].resistance;
							} 
					}
			} 
			else //(adc_value <  res_table[3].adc_value)
			{
					for(i = (size>>1); i <= (size>>1)+(size>>2); i++) //(i = 2;i<3;i++)
					{
							if(res_table[i-1].adc_value < adc_value && res_table[i].adc_value >= adc_value) 
							{
									return res_table[i].resistance;
							}
					}
			}
	} 
	else if(adc_value > res_table[size>>2].adc_value) 
	{
			for(i = (size>>2); i <= (size>>1); i++) 
			{
					if(res_table[i-1].adc_value < adc_value && res_table[i].adc_value >= adc_value) 
					{
							return res_table[i].resistance;
					} 
			}
	} 
	else 
	{
			for(i = 0; i <= (size>>2); i++) 
			{
					if(0 == i) 
					{
							if( res_table[i].adc_value >= adc_value) 
							{
									return res_table[i].resistance;
							}
					} 
					else //i = 1
					{
							if(res_table[i-1].adc_value < adc_value && res_table[i].adc_value >= adc_value) 
							{
									return res_table[i].resistance;
							}
					}
			}
	}
	return RGBY_DATA_MAX;//if the process is correct, it won't run this line.
}

#endif





uint16_t Get_Adc_Value(void)
{
    uint32_t  adc_value_total = 0;
    int i=0;
    for(i=0;i<8;i++)
        {
            adc_value_total += adc_Buffer[i];
        }
    
    adc_value_total = adc_value_total >> 3;    

    return 0xffff&adc_value_total;
}















