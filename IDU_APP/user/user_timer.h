
#ifndef __USER_TIMER_H__
#define __USER_TIMER_H__



#include "stm8l15x.h"


extern __IO uint32_t System_Tick;




extern uint16_t Time2_count;

extern uint16_t Time3_count;






void User_Tim1_Init(void);

void User_Tim2_Init(void);

void User_Tim3_Init(void);

void User_TIM3_For_PWM(void);
void User_Set_PWM_Value(uint16_t value);


void Delay_Ms(uint32_t ms);



void  Get_System_Tick(uint8_t *buf);


void User_Tim1_Init_For_DIFF_RG_Y(void);
void User_Tim5_Init_For_Spoke(void);




#define delayms(X)  delayms_time2(X)


void User_Tim4_For_Uart_Timeout_Handle(void);

void User_Tim3_For_Y_SYNC(void);



#endif




