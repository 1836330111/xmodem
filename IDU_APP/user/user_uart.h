
#ifndef __USER_UART_H__
#define __USER_UART_H__





#include "stm8l15x_usart.h"





#define  B115200 0
#define  B57600  1
#define  B38400  2
#define  B28800  3
#define  B19200  4
#define  B9600   5





void User_Uart1_Int(int baud_rate_enum);



void User_Uart2_Int(int baud_rate_enum);


void User_Uart3_Int(int baud_rate_enum);


void print_buf(uint8_t* buf,int len);



int Uart_Send_Buf(USART_TypeDef * Port,uint8_t* buf,int16_t len);

uint8_t USART1_Receive(uint8_t* ReceiveData,uint32_t Timeout);//串口1接收数据 超时退出


#endif










