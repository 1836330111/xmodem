#ifndef __DECODE_H_
#define __DECODE_H_


#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "ppfifo.h"

#define CMD_SIZE 128



#ifndef     __IO
#define     __IO    volatile         /*!< defines 'read / write' permissions  */
#endif

typedef struct _DECODE_TABLE
{

	//decode logic
	//transport
	
	__IO uint8_t 	part;
    
    __IO uint8_t	flag;
	__IO uint8_t	route_from; 
	__IO uint8_t	route_to;   
	__IO uint8_t	packet_count;




    __IO uint8_t 	check_sum;
	__IO uint16_t   packet_len;
	__IO uint16_t	recv_total;
	uint8_t         package_buf[CMD_SIZE];




	pppfifo_t pf;


	//�����ɹ���
	__IO uint8_t	cmd_len;
	__IO uint8_t	cmd_flag;
	uint8_t		    cmd_buf[CMD_SIZE];



	
}DECODE_TABLE, *pDECODE_TABLE;



//extern DECODE_TABLE Decode_Table;
extern DECODE_TABLE Decode_Lx;


unsigned char Make_EB90_Sum_Ext(unsigned char start_value,unsigned char * buf ,int len);



unsigned char Check_Sum_EB90(unsigned char * buf ,int len);



int Make_Sum_EB90(unsigned char * buf ,int len,unsigned char* SumBuf);



#endif






