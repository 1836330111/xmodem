


#include "public.h"
#include "decode.h"
//DECODE_TABLE Decode_Table;
DECODE_TABLE Decode_Lx;
//---------------------------------------------------------------------------------------------------
//      chr        : 从数据流中取出一个数据（一般从FIFO中取出一个数据）
//  decode_table   : 解码结构体
//     buf         : 解码成功，命令包放入buf 
//   return        : 返回命令包长
//---------------------------------------------------------------------------------------------------


//#define PRINT_DECODE  printf
#define PRINT_DECODE // printf

int CMD_Lx_Decode(pDECODE_TABLE decode_table, unsigned char chr,unsigned char * buf)
{        
	//PRINT_DECODE("%s,part:%x :%X\r\n",__FUNCTION__,decode_table->part,chr);
	//printf("%02x ",chr);
	PRINT_DECODE("%x ",chr);
    switch(decode_table->part)
    {
        case 0 :    // 同步
            {
                if(((chr&0x8C) == 0x8C) )
                {
                    decode_table->package_buf[decode_table->recv_total] = chr;
                    decode_table->recv_total++;
		    		PRINT_DECODE("sys in \r\n"); 
                }             
                else
                {
                    decode_table->part = 0;
                    decode_table->recv_total = 0;

                    PRINT_DECODE("sys err \r\n");                     
                    return 0;
                }

		  if(decode_table->recv_total<1)return 0;
				
                if(((decode_table->package_buf[0]&0x8C) == 0x8C))
                {
                    decode_table->part = 1;
                    
                    //decode_table->package_buf[0] = 0xEB;
                    //decode_table->package_buf[1] = 0x00;
                    decode_table->recv_total = 1;

		     		PRINT_DECODE("sys ok\r\n");
                    return 0;
                }
			else
				{

                decode_table->part = 0;
                decode_table->recv_total = 0;

				}
            }
            break;
			

        case 1:// 取数据
            { 
                decode_table->package_buf[decode_table->recv_total++] = chr;
                
                PRINT_DECODE("data in %x %x\r\n",decode_table->packet_len,decode_table->recv_total);

            
                if(decode_table->recv_total >= 19)  // 数据取完，也即一个包取完
                {   
					decode_table->part = 2;
					PRINT_DECODE("data ok \r\n");
                    return 0;
                }

               
				break;
                
            }
        case 2 : // 校验和;
            { 
                decode_table->package_buf[decode_table->recv_total] = chr;

				decode_table->check_sum = Check_Sum_EB90(decode_table->package_buf,19);

                PRINT_DECODE("check_sum:%x \r\n",decode_table->check_sum);
				
				// 数据取完，也即一个包取完
                if(decode_table->package_buf[decode_table->recv_total]==decode_table->check_sum) 
                {
                
				PRINT_DECODE("check ok len :%d \r\n",decode_table->recv_total);

				decode_table->cmd_flag = 1;
				decode_table->cmd_len = 20;
				memcpy(decode_table->cmd_buf, &decode_table->package_buf[0],20);
			

					// 清所有变量
					
				memset(decode_table->package_buf,0,20); 
				decode_table->part = 0;
				decode_table->recv_total = 0;
				return 20;
                    
			}
			else
			{
				// 清所有变量
				
                memset(decode_table->package_buf,0,decode_table->recv_total); 
				decode_table->part = 0;
				decode_table->recv_total = 0;
				PRINT_DECODE("check sum err\r\n");
				return 0;
			}
			
  			break;
                }
		
        default:
			{
			PRINT_DECODE("default clean\r\n");
			decode_table->part = 0;
			decode_table->recv_total = 0;
			decode_table->packet_len = 0;
            break;  
        	}
    } 
    
    return 0;
}





#if 0
int CMD_EB90_Decode(pDECODE_TABLE decode_table, unsigned char chr,unsigned char * buf)
{        
	//PRINT_DECODE("%s,part:%x :%X\r\n",__FUNCTION__,decode_table->part,chr);
	//printf("%02x ",chr);
	//PRINT_DECODE("%x ",chr);
    switch(decode_table->part)
    {
        case 0 :    // 同步
            {
                if((chr == 0xEB) || (chr == 0x90))
                {
                    decode_table->package_buf[decode_table->count] = chr;
                    decode_table->count ++;
					PRINT_DECODE("sys in \r\n"); 
                }             
                else
                {
                    decode_table->part = 0;
                    decode_table->count = 0;
                    decode_table->data_len = 0;
                    PRINT_DECODE("sys err \r\n");                     
                    return 0;
                }

		  if(decode_table->count<2)return 0;
				
                if((decode_table->package_buf[decode_table->count-2]==0xEB)  && (decode_table->package_buf[decode_table->count-1]==0x90))
                {
                    decode_table->part = 1;
                    decode_table->data_len = 0;
                    
                    decode_table->package_buf[0] = 0xEB;
                    decode_table->package_buf[1] = 0x90;
                    decode_table->count = 2;

					PRINT_DECODE("sys ok\r\n");
                    return 0;
                }
            }
            break;
			
		case 1 :	// 取数命令 和长度;
		{
			// printf("cmd and len in\r\n");
			 decode_table->package_buf[decode_table->count] = chr;
			 decode_table->count ++;


             if(decode_table->count == 3)
                {
                    decode_table->route = chr;//路由位
                    PRINT_DECODE("route %x \r\n",chr);
                }
			 else if(decode_table->count == 4)
			 {
					decode_table->part = 2;

					decode_table->data_len = chr;  //decode_table->package_buf[2];

				 if((decode_table->data_len > 240) )	 //防止数据量太大 或 无数据
				 {
					decode_table->part = 0;
					decode_table->count = 0;
					decode_table->data_len = 0;  
					PRINT_DECODE("len err\r\n");
				 }
                    PRINT_DECODE("len %x\r\n",decode_table->data_len);
                    return 0;
			  }
		break;	 
		} 
				 

        case 2:// 取数据
            { 
                decode_table->package_buf[decode_table->count] = chr;
                decode_table->count ++;
                PRINT_DECODE("data in %x %x\r\n",decode_table->data_len,decode_table->count);

                //if(decode_table->data_len <= decode_table->count+1)   // 数据取完，也即一个包取完
				if(decode_table->count+1 >= decode_table->data_len+5)   // 数据取完，也即一个包取完
                {   
					decode_table->part = 3;
					PRINT_DECODE("data ok \r\n");
                    return 0;
                }
				break;
            }
        case 3 : // 校验和;
            { 
                decode_table->package_buf[decode_table->count] = chr;
                //decode_table->count++;
				decode_table->check_sum = Check_Sum_EB90(decode_table->package_buf,decode_table->count);
                printf("check_sum:%x ",decode_table->check_sum);
                if((decode_table->package_buf[decode_table->count]==0)||\
                   (decode_table->package_buf[decode_table->count]==decode_table->check_sum)) // 数据取完，也即一个包取完
                {
                
				PRINT_DECODE("check ok len :%d \r\n",decode_table->data_len);
				if(0)//buf != 0)
					{
					// 将数据包 copy 到 unmap
					memcpy(buf, &decode_table->package_buf[4],decode_table->data_len);
					PRINT_DECODE("decode ok to buf\r\n");
					}
				else
					{
					decode_table->cmd_flag = 1;
					decode_table->cmd_len = decode_table->data_len;
					memcpy(decode_table->cmd_buf, &decode_table->package_buf[4],decode_table->data_len);
					PRINT_DECODE("decode ok to struct\r\n");
					}

					// 清所有变量
					
					memset(decode_table->package_buf,0,decode_table->count); 
					decode_table->part = 0;
					decode_table->count = 0;
					return decode_table->data_len;
					//return decode_table->count;
			}
			else
			{
				// 清所有变量
				
                memset(decode_table->package_buf,0,decode_table->count); 
				decode_table->part = 0;
				decode_table->count = 0;
				PRINT_DECODE("check sum err\r\n");
				return 0;
			}
			
  			break;
                }
		
        default:
			{
			PRINT_DECODE("default clean\r\n");
			decode_table->part = 0;
			decode_table->count = 0;
			decode_table->data_len = 0;
            break;  
        	}
    } 
    
    return 0;
}



#endif


unsigned char Make_EB90_Sum_Ext(unsigned char start_value,unsigned char * buf ,int len)
{
	int sum=0;
	int i=0;

    sum=start_value;
    
	for(i=0;i<len;i++)
	{
		//printf("%02x ",buf[i]);
		sum=buf[i]+sum;
	}
	 //printf("\r\nsum:%x \r\n",sum);
	return sum&0xFF;
}



unsigned char Check_Sum_EB90(unsigned char * buf ,int len)
{
	int sum=0;
	int i=0;
	
	for(i=0;i<len;i++)
	{
		//printf("%02x ",buf[i]);
		sum=buf[i]+sum;
	}
	 //printf("\r\nsum:%x \r\n",sum);
	return sum&0xFF;
}


int Make_Sum_EB90(unsigned char * buf ,int len,unsigned char* SumBuf)
{
	int sum=0;
	int i=0;
	for(i=0;i<len;i++)
	{
		sum=buf[i]+sum;
	}

	 SumBuf[0]=(sum&0xff00)>>8;
	 SumBuf[1]=(sum&0xff);

	return sum&0xFFFF;
}




