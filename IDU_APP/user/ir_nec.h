

#ifndef __IR_NEC_H__
#define __IR_NEC_H__

//#include "stdint.h"
#include "stm8l151c8.h"
#ifndef   __IO
#define    __IO    volatile         
#endif

#ifndef __AB(X,Y)
#define    __AB(X,Y)   ((X>Y)?(X-Y):(Y-X))
#endif


#define TRUE     1
#define FALSE    0





#define IR_SYNC_HEAD 0xFB04




#define IR_POWER_CODE 0x12ED
#define IR_MUTE_CODE  0x50AF
#define IR_KEYSTONE_UP_CODE  0x10EF
#define IR_KEYSTONE_DOWN_CODE  0x906F
#define IR_PICTURE_MODE_CODE  0x728D
#define IR_ASPECT_RATIO_CODE  0x629D
#define IR_VOLUME_UP_CODE    0x20DF
#define IR_VOLUME_DOWN_CODE  0xA05F
#define IR_BLANK_SCREEN_CODE  0xC03F
#define IR_SOUCE_IN_CODE  0x7887
#define IR_MOVE_UP_CODE  0xE21D
#define IR_MOVE_DOWN_CODE  0xB24D
#define IR_MOVE_LEFT_CODE  0x926D
#define IR_MOVE_RIGHT_CODE  0xD22D
#define IR_MENU_CODE  0x52AD
#define IR_RETURN_CODE  0xC837

#define IR_KEY_1_CODE  0xFF00
#define IR_KEY_2_CODE  0x08F7
#define IR_KEY_3_CODE  0x8877
#define IR_KEY_4_CODE  0x28D7
#define IR_KEY_5_CODE  0xA857
#define IR_KEY_6_CODE  0xE817
#define IR_KEY_7_CODE  0x9867
#define IR_KEY_8_CODE  0xD827
#define IR_KEY_9_CODE  0x38C7

#define IR_ENERGY_SAVE_CODE  0x827D
#define IR_MICROPHONE_CODE   0x02FD






















///////////////////  IR  ////////////////////////////////////////////


extern __IO uint16_t Ir_Time_Isr100us;


extern uint32_t   bBitCounter;
extern uint16_t   IrdaRX;
extern uint16_t   Ir_Repeat_GAP;
extern uint16_t   IrdaRX_repeat_cur;
extern uint8_t   irda_buff_changed;


void IR_NEC_KEY_ISR(void);


void Ir_Bsp_Init(void);



#endif



