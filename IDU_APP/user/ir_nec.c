

#include "public.h"


__IO uint16_t Ir_Time_Isr100us=0;



uint32_t    bBitCounter=0;
uint16_t    IrdaRX=0;
uint16_t    Ir_Repeat_GAP=0;
uint16_t    IrdaRX_repeat_cur =0;
uint8_t     irda_buff_changed =0;

unsigned char IrdaRXbuf[10]={0};


void print_ir(unsigned char* buf,int len)
{

	printf("%02x %02x %02x %02x\r\n",IrdaRXbuf[0],IrdaRXbuf[1],IrdaRXbuf[2],IrdaRXbuf[3]);
}




void IR_NEC_KEY_ISR(void)
{

	static __IO unsigned char  startCheck = FALSE;
	static __IO unsigned char  CheckTimes=0;

	if(__AB(Ir_Time_Isr100us,0x7F) <=15)// 13.5 ms    13500/100=135
	{    
		// head
		bBitCounter=0;
		startCheck=TRUE;
		CheckTimes=0;
		//printf("H");
		memset(IrdaRXbuf,0,4);

	}
	else if((startCheck==TRUE)&&(__AB(Ir_Time_Isr100us,0x16) <=5))// 2.24 ms    2240/100=22
	{      // 1
		IrdaRXbuf[bBitCounter/8] = IrdaRXbuf[bBitCounter/8]<<1;
		IrdaRXbuf[bBitCounter/8] = IrdaRXbuf[bBitCounter/8]+0x01;
		bBitCounter++;
		//printf("1");
	}
	else if((startCheck==TRUE)&&(__AB(Ir_Time_Isr100us,0x0A) <=4))// 1.12 ms    1120/100=11
	{      // 0
		IrdaRXbuf[bBitCounter/8] = IrdaRXbuf[bBitCounter/8]<<1;
		bBitCounter++;
		//printf("0");
	}
	else if((startCheck==TRUE)&&((__AB(Ir_Time_Isr100us,0x180) <=15)||(__AB(Ir_Time_Isr100us,0x399) <=15)))
	{      //repeat 1
		//printf("repeat1\r\n");
		//printf("R");

	}
	else if((startCheck==TRUE)&&(__AB(Ir_Time_Isr100us,0x6A) <=8))
	{
	//repeat 2
	//printf("repeat2\r\n");
	//printf("r");
	    bBitCounter = 0;
	    if(Ir_Repeat_GAP>190) IrdaRX=0;
	    else Ir_Repeat_GAP=0;
	    if(IrdaRX)
	    {

	    IrdaRX_repeat_cur++;
	    irda_buff_changed=1;
	    }
	}
	else if(startCheck==TRUE)
	{
		if(CheckTimes>=30)
			{
			
	        startCheck=FALSE;
	        bBitCounter=0;
			
			CheckTimes=0;
			}
		else
			{
			//printf("E\r\n");
			CheckTimes++;
			}
	}

	if((startCheck==TRUE)&&(bBitCounter==32))
	{
		//printf("%x %x %x %x \r\n",IrdaRXbuf[0],IrdaRXbuf[1],IrdaRXbuf[2],IrdaRXbuf[3]);
		print_ir(IrdaRXbuf,4);
		bBitCounter=0; //clear bit count


		if((IrdaRXbuf[0]==((IR_SYNC_HEAD&0xff00)>>8))&& \
			IrdaRXbuf[1]==(IR_SYNC_HEAD&0xff))
			{
				//工厂码匹配成功
				memcpy((unsigned char*)&IrdaRX,&IrdaRXbuf[2],2);
				
				irda_buff_changed = 1;
				IrdaRX_repeat_cur=0;
				Ir_Repeat_GAP=0;
			}
			else
			{
				IrdaRX = 0;
			}
			//memset(IrdaRXbuf,0,4);	// clear recv fifo	
	}
	
	//printf("%x ",Ir_Time_Isr100us);
	Ir_Time_Isr100us = 0;
	
}




//Ir_Time_Isr100us 这个变量放入自加模块，
//100us加1

void Ir_Bsp_Init(void)
{
	//下降沿中断


	EXTI_SetPinSensitivity(EXTI_Pin_1,EXTI_Trigger_Falling);
	EXTI_SelectPort(EXTI_Port_B);	 
	GPIO_Init(GPIOB , GPIO_Pin_1, GPIO_Mode_In_FL_IT);	 // ir in

}








