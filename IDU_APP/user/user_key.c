

#include "public.h"



#define USER_KEY_GPIO   GPIOB
#define USER_KEY_PIN 	GPIO_Pin_7
#define USER_KEY_MODE   GPIO_Mode_In_PU_No_IT


extern __IO uint32_t System_Tick;

#define KEY_GET_SYSTEM_TICKET  System_Tick







void User_Init_Key(void)
{

	GPIO_Init(USER_KEY_GPIO ,USER_KEY_PIN , USER_KEY_MODE);	 // power key


}





uint8_t Get_Key_State(void)
{
	BitStatus  temp;

	temp=GPIO_ReadInputDataBit(USER_KEY_GPIO , USER_KEY_PIN);

	return (temp&USER_KEY_PIN)?1:0;
}







int Key_Handle(void)
{

	static uint32_t old_tick=0;
	static uint8_t key_active_flag = 0;
	if(0==Get_Key_State())
		{
		if(key_active_flag==0)
			{
				old_tick = System_Tick;
				key_active_flag=1;
			}
		
		if(System_Tick-old_tick > 100)
			{
				key_active_flag = 0;
				return 1;
			}
		else
			{
				return 0;	
			}

		}
	else
		{
			key_active_flag = 0;
			return 0;
		}





	

}








