#ifndef _PPFIFO_H_
#define _PPFIFO_H_
#ifdef __cplusplus
extern "C" {
#endif

#pragma pack (1)
  
typedef struct ppfifo
{
    unsigned char  mode;
    unsigned int   buffer_toal;
    unsigned int   buffer_len_1;
    unsigned int   buffer_len_2;
    unsigned char* buffer_1;    
    unsigned char* buffer_2;    
    unsigned char* ppdata;
	 
}ppfifo_t,*pppfifo_t;



extern pppfifo_t g_pppfifo;




ppfifo_t* ppfifo_alloc(unsigned int vsize);
void ppfifo_free(ppfifo_t *ppfifo);
unsigned int ppfifo_in(ppfifo_t *fifo,unsigned char *buf, unsigned int len);
unsigned int ppfifo_out(ppfifo_t *fifo,unsigned char *buf);

#pragma pack ()

#ifdef __cplusplus
}
#endif

#endif

